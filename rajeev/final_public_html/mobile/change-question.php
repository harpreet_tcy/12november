<?php
include"config.php";
include_once($_SERVER['DOCUMENT_ROOT'].'/superadmin_new/imageFunc.php');
//include_once($_SERVER['DOCUMENT_ROOT'].'/Analyse/questionview-function.php');
include_once(constant("ANALYSE") . "/analyseCommon.cls.php");
$commonAnaObj = new analyseCommon();
include('includes/testfunc.php');
$qno = '1';
//header("Content-Type: text/html; charset=utf8\n");
header("Content-Type: text/html; charset=iso-8859-1");
//header("Content-Type: text/html; charset=Windows-1256\n");
$optionInTest=0;
if ($_REQUEST['pNo'] != '')
    $qno = $_REQUEST['pNo'];
//    error_reporting(E_ALL);  ini_set('error_reporting', E_ALL); ini_set('display_errors', 'On');
?>

<style>
    .QuesByQuesName img{
        max-width: 100%;
        height: auto;
    }
    ul.jp-controls li {
            width: 50% !important;
    }
    .hiderview{
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,ffffff+100&0+0,0.79+44,1+100 */
        background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,0.79) 44%, rgba(255,255,255,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%,rgba(255,255,255,0.79) 44%,rgba(255,255,255,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(255,255,255,0) 0%,rgba(255,255,255,0.79) 44%,rgba(255,255,255,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
        z-index:1;
        position:relative;
        /*margin-top:-35px;
        height:35px;*/
        }


        .overFlowQues{
        max-height:70px;
        overflow:hidden;
        } 
</style>
<!--FOR IELTS MOCK TESTS-->
<script type="text/javascript" src="https://www.tcyonline.info/onlinefiles/jPlayer/jquery.jplayer.min.js?_=<?php time(); ?>"></script>
<script type="text/javascript" src="https://www.tcyonline.info/onlinefiles/jPlayer/playerListPlugin.js?_=<?php time(); ?>"></script>
<script type="text/javascript" >
    function cofunction(){
    // interface_obj.log("log works fine cofunction");
       
        $('#expand').show();
        $('.hiderview').show();
        $('#collapse').hide();
        $('.questionHtml').removeClass('overFlowQues').addClass('overFlowQues');
    }

    function yofunction(){
       
        $('#collapse').show();
        $('#expand').hide();
        $('.hiderview').hide();
        $('.questionHtml').removeClass('overFlowQues');
    } 
</script>

<body onLoad="showQueNext('<?= $testtakenid ?>', <?= $qno ?>, '<?=$testid?>')">

    <div style="float: left;width: 100%" id="QuesNo" ></div>
    <?php
    if ($_REQUEST['action'] == 'Quechange' && $_REQUEST['pNo'] != '' && $_REQUEST['takenId'] != '') {
        $Qn = $_REQUEST['pNo'];
        $testtakenid = $_REQUEST['takenId'];
        $testid=$_REQUEST['testId'];
        $secQuesArr = array();

        $whichCategory = getTtakenWhcihCat($testtakenid);
        
        $ttakenDetail = getTtakenDetail($testtakenid);
        $TestLang = $ttakenDetail['lang_id'];

        $optionInTest = $commonObj->optionsInTest($testid,$whichCategory);

        $getSectionDetailsQry = $commonObj->sql_query("SELECT count(*) as secQuesCount, sectionid as secNoVal FROM ".QUES_ANALYSE_TABLE."  WHERE testtakenid = '" . $testtakenid . "' GROUP BY sectionid");
        while ($getSectionDetailsRes = $commonObj->sql_fetch_assoc($getSectionDetailsQry)) {
            extract($getSectionDetailsRes);
            $secQuesArr[$secNoVal] = $secQuesCount;
        }
        $quesCount = array_sum($secQuesArr);

        $getQuestions = $commonObj->sql_query("SELECT *  FROM ".QUES_ANALYSE_TABLE." WHERE `testtakenid` ='" . $testtakenid . "' limit " . ($Qn - 1) . ", 1 ");
        $getQuestionsRow = $commonObj->sql_fetch_assoc($getQuestions);
        
        
        $eTestTbl = 'e_test';
        $eTestTblId = 'id';
        
        if( $TestLang>1 ) {
            $eTestTbl = 'e_test_lang';
            $eTestTblId = 'e_test_id';
        }
        
        $ques_html = $commonObj->sql_query("select * from ".$eTestTbl." where ".$eTestTblId."='" . $getQuestionsRow['qid'] . "'");
        $ques_htmlrow = $commonObj->sql_fetch_assoc($ques_html);
 
      
           if($ques_htmlrow['ans1'] == '')
           {
           

                $where = $ques_htmlrow['f_id']==21 || $ques_htmlrow['f_id']==22?"":'';     
                if($ques_htmlrow['f_id']==21 || $ques_htmlrow['f_id']==22)
                {

                    $get_options = $commonObj->sql_query('select GROUP_CONCAT(id) as id_list from e_test_answers where e_test_id = '.$getQuestionsRow['qid'].' GROUP by group_ans');

                 
                             $index = 1;
                            // $where = $ques_htmlrow['f_id']==21 || $ques_htmlrow['f_id']==22?" and ans_num = 0":''; 
                             
                              while($anwer_options1 = $commonObj->sql_fetch_array($get_options))
                              {
                                
                                 $getalloptions = $commonObj->sql_query('select * from e_test_answers where id in ('.$anwer_options1['id_list'].')');
                                 while($anwer_options = $commonObj->sql_fetch_array($getalloptions))
                                 {
                                    $ans_option = html_entity_decode($anwer_options['ans_text']);   
                                    $ans_arr[$index][] = $ques_options = setImageData($ans_option, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
                                       
                                 }
                                  $index++;
                              }

                            

                       
                }
                else
                {
                    $get_options = $commonObj->sql_query('select * from e_test_answers where e_test_id = '. $getQuestionsRow['qid'].' '.$where);
                    $answer_num = $commonObj->sql_num_rows($get_options);
                    if($answer_num > 0)
                    {
                         $index = 1;
                         while($anwer_options = $commonObj->sql_fetch_array($get_options))
                         {  
                                $ans_option = html_entity_decode($anwer_options['ans_text']);   
                                $ans_arr[$index] = $ques_options = setImageData($ans_option, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
                                $index++;
                         }
                         
                    }
                }
                    
                    
            }
       

        $ans1 = $ques_htmlrow['ans1'];
        if ($ans1 != ''){
            $ans_arr['1'] = $ques_options = setImageData($ans1, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//            if(!strstr($ans1, 'onlinetest')){
//                $ans1=str_replace('src="', 'src="/onlinetest/'.$ques_htmlrow['folder_name'].'/', $ans1);
//            }
//            $ans_arr['1'] = $ques_options = '<font size="2" face="Arial">' . $ans1 . '</font>';
        }
        $ans2 = $ques_htmlrow['ans2'];
        if ($ans2 != ''){
            $ans_arr['2'] = $ques_options = setImageData($ans2, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//            if(!strstr($ans2, 'onlinetest')){
//                $ans2=str_replace('src="', 'src="/onlinetest/'.$ques_htmlrow['folder_name'].'/', $ans2);
//            }
//            $ans_arr['2'] = $ques_options = '<font size="2" face="Arial">' . $ans2 . '</font>';
        }
        $ans3 = $ques_htmlrow['ans3'];
        if ($ans3 != ''){
            $ans_arr['3'] = $ques_options = setImageData($ans3, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//            if(!strstr($ans3, 'onlinetest')){
//                $ans3=str_replace('src="', 'src="/onlinetest/'.$ques_htmlrow['folder_name'].'/', $ans3);
//            }
//            $ans_arr['3'] = $ques_options = '<font size="2" face="Arial">' . $ans3 . '</font>';
        }
        if( isset($optionInTest) && $optionInTest<4 && $optionInTest >0  ) {
            unset( $ques_htmlrow['ans4']);
            unset( $ques_htmlrow['ans5']);
        }else  if( isset($optionInTest) && $optionInTest<5 && $optionInTest >0) {
            unset( $ques_htmlrow['ans5']);
        }
        $ans4 = $ques_htmlrow['ans4'];
        if ($ans4 != ''){
            $ans_arr['4'] = $ques_options = setImageData($ans4, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//            if(!strstr($ans4, 'onlinetest')){
//                $ans4=str_replace('src="', 'src="/onlinetest/'.$ques_htmlrow['folder_name'].'/', $ans4);
//            }
//            $ans_arr['4'] = $ques_options = '<font size="2" face="Arial">' . $ans4 . '</font>';
        }
        $ans5 = $ques_htmlrow['ans5'];
        if ($ans5 != ''){
            $ans_arr['5'] = $ques_options = setImageData($ans5, $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//            if(!strstr($ans5, 'onlinetest')){
//                $ans5=str_replace('src="', 'src="/onlinetest/'.$ques_htmlrow['folder_name'].'/', $ans5);
//            }
//            $ans_arr['5'] = $ques_options = '<font size="2" face="Arial">' . $ans5 . '</font>';
        }


        ?>

        <?php
        $statusQue = '';
        if ($getQuestionsRow['result'] == '1') {
            $statusQue = '<a style="color:#43750D;text-decoration:none;">Correct</a>';
            $class_Que='correctAns';
        } else if ($getQuestionsRow['result'] == '2') {
            $statusQue = '<a style="color:#FF0000;text-decoration:none;">Incorrect</a>';
            $class_Que='incorrectAns';
        } else if ($getQuestionsRow['result'] == '0') {
            $statusQue = '<a style="color:#878314;text-decoration:none;">Unattempted</a>';
            $class_Que='unattemptedAns';
        }
        
        //$Question=$ques_htmlrow['question'];
       $Question = '';
       $passage_url = TCYSITEURL."/onlinetest/".$ques_htmlrow['folder_name'].'/'.$ques_htmlrow['passage'];

        if($ques_htmlrow['passage']!='')
        {
            $question_passage = file_get_contents($passage_url);
            $Question = '<div class="questionHtml overFlowQues">'.$question_passage.'</div><div class="hiderview">&nbsp;</div><center style="margin-bottom: 10px;" ><a href="#" id="expand" onclick="yofunction()" class="btn"  style=" margin:5px auto;"><font size=2 face=Arial >See all</font> <img src="/mobile/testplatform/arrow.png" style="margin-left:5px;" height="8px" width="14px" /> </a> <a href="#" id="collapse" onclick="cofunction()" class="btn" style="margin:5px auto; display:none;"> <font size=2 face=Arial>See less</font> <img src="/mobile/testplatform/arrow1.png" style="margin-left:5px;" height="8px" width="14px" /> </a></center>';
        }

        $Question .= setImageData($ques_htmlrow['question'], $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
//        if(!strstr($Question, 'onlinetest')){
//            $Question=str_replace('src="', 'src="/onlinetest/'.$testid.'/', $Question);
//        }
             
                 
        /*    
        if(strlen($Question) == 0)
        {

                $passage_url = TCYSITEURL."/onlinetest/".$ques_htmlrow['folder_name'].'/'.$ques_htmlrow['passage'];

               // mail('s.harpreet@tcyonline.org','url',$passage_url);
                $question_passage = file_get_contents($passage_url);
                $Question = $question_passage;
        } */

    $audioData='';
       if($ques_htmlrow['audio_sol']!='')
       {
      
            $audioPath = '/onlinetest/'.$ques_htmlrow['folder_name'].'/'.$ques_htmlrow['audio_sol'];
           $que_audio_text = '';
                        if($f_id==43)
                        {
                            $que_audio_text = 'Lecture Audio:';
                        }else
                        {
                            $que_audio_text = 'Question Audio:';
                        }

                    $audioData = '<div align="center" id="audioDiv" class="loader">
                            <div id="qseq" style="float:left;">Question</div>
                            <div align="right" class="VideoClose"><a href="javascript:closeDullDiv(\'audioDiv\');"><img width="15" height="15" border="0" alt="close" src="'.constant('TCYSITEURL').'/india/images/close_pop.gif"></a></div>
                            <div id="explContent" style="overflow:auto; width:625px; background:#fff; color:#333; height:400px;"></div>
                        </div>
                        <link href="/onlinefiles/jPlayer/jplayer.pink.flag.css" rel="stylesheet" type="text/css" />
                        <div class="clear">&nbsp;</div>
                        <div style="float:none; margin-bottom:10px;" class="QBYQ_question">'.$que_audio_text.'</div>
                        <div id="jpId"></div>
                        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                        <div id="jp_container_1" class="jp-audio">
                            <div class="jp-type-single">
                                <div class="jp-gui jp-interface">
                                    <ul class="jp-controls">
                                        <li><a href="javascript:;" class="jp-play" onclick="fullPlayerListener(\'' . $audioPath . '\',\'' . $ques_htmlrow['id'] . '\', \'jquery_jplayer_1\');" tabindex="1">play</a></li>
                                        <li><a href="javascript:;" class="jp-pause" tabindex="1" style="display:none;">pause</a></li>
                                        <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
                                        <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
                                        <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
                                        <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
                                    </ul>
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-bar">
                                        <div class="jp-volume-bar-value"></div>
                                    </div>
                                    <div class="jp-current-time"></div>
                                    <div class="jp-duration"></div>
                                    <!--<ul class="jp-toggles">
                                            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
                                            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
                                    </ul>-->
                                </div>
                            </div>
                        </div>';

       }


        $explanation = setImageData($ques_htmlrow['text'], $testid, $ques_htmlrow['id'], $ques_htmlrow['folder_name']);
        $QuesStr = '';
        $QuesStr = '<div style="float: left;width:100%;margin-bottom: 20px;padding: 5px;box-shadow: 0 0 5px #CCCCCC;" id="QuesNo' . $Qn . '" >
                    <div class="question_n_cont">
                        <div style="padding-left:0px" class="wrapper">
                            <div class="q_no">' . $Qn . ' of ' . $quesCount . '</div>
                            <div class="FloatRight">' . $statusQue . '</div>
                        </div>
                    </div>
			<div class="QuesByQuesName">' . $Question . '</div>
            <div class="quesAudio">'.$audioData.'</div>
			<div class="QuesByQuesAnsList">';
        $checkboxfid = array('1','9');
        $checkAnsArr = array();
        $checkCorrArr = array();
        
        if(in_array($ques_htmlrow['f_id'], $checkboxfid))
        {
            $get_quesdata = $commonObj->sql_query("SELECT * FROM ques_analysis_new where testtakenid = '".$testtakenid."' and qid = '".$getQuestionsRow['qid']."'");
                $quesdata = $commonObj->sql_fetch_array($get_quesdata);
                $my_ans = str_replace('^', ',', $quesdata['ans_given']);
                $correct_ans = str_replace('^', ',', $quesdata['correct_ans']);
                
                $checkAnsArr = explode(',', $my_ans);
                $checkCorrArr = explode(',', $correct_ans);
                
//                echo '<pre>';
//                print_r($checkAnsArr);
//                echo '<pre>';
//                print_r($checkCorrArr);
//              
//              die;
                $QuesStr.='<style>
                                .answer_icon
                                {
                                    background:url("/mobile/images/answer_square.jpg") no-repeat;
                                }
                         </style>';
        }
        foreach ($ans_arr as $key => $answers) {
            $result = $getQuestionsRow["result"];
            $ansGiven = $getQuestionsRow["ans_given"];
            $correctAns = $getQuestionsRow["correct_ans"];
          
            if ($result == '1' && $ansGiven == $key) {
                $classname = 'correct_ans';
                $selVal = 'style="background-color:#729959; color:#fff;"';
            } else if ($result == '2' && $ansGiven == $key) {
                $classname = 'wrong_ans';
                $selVal = 'style="background-color:#FFFEE6; color:#B70000;"';
            } else if ($result == '0' && $correctAns == $key) {
                $classname = 'correct_ans';
                $selVal = 'style="background-color:#729959; color:#fff;"';
            } else {
                $classname = 'answer_icon';
                $selVal = '';
            }
            if ($result == '2' && $correctAns == $key)
            {
                $classname = 'correct_ans';
                $selVal = 'style="background-color:#729959; color:#fff;"';
            }
                
            if($ques_htmlrow['f_id']!=22 && $ques_htmlrow['f_id']!=21 && $ques_htmlrow['f_id']!=69 && $ques_htmlrow['f_id']!=36 && !in_array($ques_htmlrow['f_id'], $checkboxfid) )
            {
                if(($ques_htmlrow['f_id']==20 || $ques_htmlrow['f_id']==27 )&& $_SERVER['REMOTE_ADDR'] == '202.191.175.186')
                {
                    
                }
                else{
                    $QuesStr.='<div class="' . $classname . '">' . $answers . '</div>';
                }
                
             }
             if( $ques_htmlrow['f_id'] == 69)
             {
                 $QuesStr.='<span '.$selVal.' >' . $answers . '</span>';
             }
             
             if(in_array($ques_htmlrow['f_id'], $checkboxfid))
             {
                 if (in_array($key, $checkAnsArr)) {
                    $classname = 'correct_ans';
                    
                } else if (!in_array($key, $checkCorrArr) && in_array($key, $checkAnsArr)) {
                    $classname = 'wrong_ans';
                    
                } else if (in_array($key, $checkCorrArr) && !in_array($key, $checkAnsArr)) {
                    $classname = 'correct_ans';
                    
                } else {
                    $classname = 'answer_icon';
                    
                }
                if (in_array($key, $checkCorrArr))
                {
                    $classname = 'correct_ans';
                    
                }
                 $QuesStr.='<div class="' . $classname . '">' . $answers . '</div>';
             }
             
        }

        if($ques_htmlrow['f_id']=='3')
        {
                 $get_quesdata = $commonObj->sql_query("SELECT * FROM ques_analysis_new where testtakenid = '".$testtakenid."' and qid = '".$getQuestionsRow['qid']."'");
                $quesdata = $commonObj->sql_fetch_array($get_quesdata);
                $my_ans = str_replace('^', ',', $quesdata['ans_given']);
                $correct_ans = str_replace('^', ',', $quesdata['correct_ans']);
                $QuesStr .='<strong>Your answers: </strong> '.$my_ans.'<br>';
                 $QuesStr .='<strong>Correct answers: </strong> '.$correct_ans.'<br>';
        }
        
            $check_fids = array('28','20','21','22','25','23','26','24');
        
          if($ques_htmlrow['f_id']==22 || $ques_htmlrow['f_id']==21)
          {

               

                 $get_quesdata = $commonObj->sql_query("SELECT * FROM ques_analysis_new where testtakenid = '".$testtakenid."' and qid = '".$getQuestionsRow['qid']."'");
                $quesdata = $commonObj->sql_fetch_array($get_quesdata);
                $my_ans = explode(",",str_replace('^', ',', $quesdata['ans_given']));

               
                $my_answers = array();
                if($_SERVER['REMOTE_ADDR'] != '202.191.175.186'){
                    foreach($ans_arr as $index=> $vals)
                    {
                         $QuesStr .= 'Blank'.$index.'<br><ul>';  
                          for($i=0;$i<count($vals);$i++)
                          {
                             $QuesStr .= '<li>'.$vals[$i].'</li>';
                             if($my_ans[$index]==$i)
                             {
                                 $my_answers[] = $vals[$i];
                             }
                          }
                          $QuesStr .='</ul>';
                    }

                    $QuesStr .='<strong>Your Answers:</strong> '.implode(',',$my_answers);

                    $QuesStr .= '<br> <style>
                                    .wrong_ans, .correct_ans
                                    {
                                        background:none;
                                    }
                             </style>';
                }
                     
                if($_SERVER['REMOTE_ADDR'] == '202.191.175.186'){
                    $f_id = $ques_htmlrow['f_id'];
                    $currentQuesId = $getQuestionsRow['qid'];
                    $lang_id = $TestLang;
                    $path = "src=/onlinetest/" . $ques_htmlrow['folder_name'] . "/wb";
                    
                    $groupOptionsFidArr = array(8, 9, 11, 17, 18, 19, 20, 21, 22,44,48,52,51,60,69,72,73,74,83,84,85,86,87,89,91,94);
                    if (in_array($f_id, $groupOptionsFidArr) && $currentQuesId != '') {

                        //if ($_SESSION["userid"] == "445677") {
                            if ($lang_id > 1) {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers_lang 
                                                            WHERE e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "' ORDER BY ans_num, group_ans");
                            } else {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                            }
                        /*} else {
                            $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                        }*/
                        if ($commonObj->sql_num_rows($GetAnsDetailsQry) > 0) {
                            while ($GetAnsDetailsRes = $commonObj->sql_fetch_assoc($GetAnsDetailsQry)) {
                                extract($GetAnsDetailsRes);
                                $ans_text = str_replace('src="src="', 'src="', $ans_text);
                                $ans_text = str_replace('"', '', $ans_text);
                                if ($ans_text != '')
                                    $ans_text = str_replace('src=wb', $path, $ans_text);
                                if ( $f_id == 8 || $f_id==51 || $f_id==52 ||  $f_id == 19 || $f_id == 17 || $f_id == 18 || $f_id == 20 || $f_id == 21 || $f_id == 22 || $f_id == 44 || $f_id == 48 || $f_id == 60 || $f_id == 73 || $f_id == 74 || $f_id == 83 || $f_id == 84 || $f_id == 85 || $f_id == 86 || $f_id == 87 || $f_id == 89 || $f_id == 91  ) {

                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if( $f_id == 72 && $GrpNo>0 ) {
                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if ( $f_id == 9 || $f_id == 50 )
                                    $AnsFidArr[$GrpNo] = $ans_text;
                                else if ($f_id == 11 || $f_id == 69)
                                    $AnsFidArr[$GrpNo] = $ans_text;
                            }
                        }
                    }
                    if ($lang_id > 1 && $currentQuesId != '') {
                        $getQuesDetailQry = $commonObj->sql_query("SELECT e_test_id as id, lang_id, tid, f_id, question, ques_search, folder_name, ans1, ans2, ans3, ans4, ans5, passage, right1, text, audio_sol FROM e_test_lang WHERE TRIM(question)!='' AND e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "'");
                    }
                    else
                    {
                        $getQuesDetailQry = $commonObj->sql_query("select a.*, b.video from e_test as a
                left join e_test_audio_video as b on a.id=b.e_test_id
                where a.id = '" . $currentQuesId . "'");
                    }
                    $getQuesDetailRow = $commonObj->sql_fetch_assoc($getQuesDetailQry);
                    $rightAns = ltrim(str_replace("^", ",", $getQuesDetailRow['right1']), ",");
                    if ($f_id == 39 || $f_id == 28) {
                        $rightAnsArr = explode(",", str_replace(", ",",",$rightAns));
                    }else{
                        $rightAnsArr = explode(",", $rightAns);
                    }

                    foreach ($rightAnsArr as $key => $val) {
                        $rightAnsArr[$key] = trim(stripslashes(strtolower($val)));
                    }
                    $currentQuesAnsGiven = $quesdata['ans_given'];
                    $currentQuesAnsGiven = ltrim(str_replace("^", ",", $currentQuesAnsGiven), ",");
                    $QuesStr .='<link href="'.constant("CDN").constant("CSS").'/analysis.css?_'.filemtime($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/analysis.css').'" rel="stylesheet" type="text/css" /><style>
                        .Normal_ans.unattempt_ans {
                                        background: #fdedc1;
                                        color: #54932f;
                                }
                            </style>';
                    $QuesStr .= $commonAnaObj->Type22Div($AnsFidArr,$rightAnsArr,$currentQuesAnsGiven);
                }
            
          }
            else if($ques_htmlrow['f_id']==27)
           {
                $get_quesdata = $commonObj->sql_query("SELECT * FROM ques_analysis_new where testtakenid = '".$testtakenid."' and qid = '".$getQuestionsRow['qid']."'");
                $quesdata = $commonObj->sql_fetch_array($get_quesdata);
               
                $Mainexplode_anwer = explode(" ",$answers);

                $my_ans = explode(",",str_replace('^', ',', $quesdata['ans_given']));
                if($_SERVER['REMOTE_ADDR'] != '202.191.175.186')
                {
                        $QuesStr .='<strong>Your answers:</strong> ';
                        $myanswer = array();

                        foreach($my_ans as $myans_data)
                        {
                                $myanswer[] = $Mainexplode_anwer[$myans_data-1];
                        }


                        $QuesStr .= implode(",",$myanswer).'<br> <style>
                                        .wrong_ans, .correct_ans
                                        {
                                            background:none;
                                        }
                                 </style>';
                }
                
               // $QuesStr .='Correct answers = '.implode(",",$correct_array).'<br>';
                if($_SERVER['REMOTE_ADDR'] == '202.191.175.186'){
                    $f_id = $ques_htmlrow['f_id'];
                    $currentQuesId = $getQuestionsRow['qid'];
                    $lang_id = $TestLang;
                    $path = "src=/onlinetest/" . $ques_htmlrow['folder_name'] . "/wb";
                    
                    $groupOptionsFidArr = array(8, 9, 11, 17, 18, 19, 20, 21, 22,44,48,52,51,60,69,72,73,74,83,84,85,86,87,89,91,94);
                    if (in_array($f_id, $groupOptionsFidArr) && $currentQuesId != '') {

                        //if ($_SESSION["userid"] == "445677") {
                            if ($lang_id > 1) {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers_lang 
                                                            WHERE e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "' ORDER BY ans_num, group_ans");
                            } else {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                            }
                        /*} else {
                            $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                        }*/
                        if ($commonObj->sql_num_rows($GetAnsDetailsQry) > 0) {
                            while ($GetAnsDetailsRes = $commonObj->sql_fetch_assoc($GetAnsDetailsQry)) {
                                extract($GetAnsDetailsRes);
                                $ans_text = str_replace('src="src="', 'src="', $ans_text);
                                $ans_text = str_replace('"', '', $ans_text);
                                if ($ans_text != '')
                                    $ans_text = str_replace('src=wb', $path, $ans_text);
                                if ( $f_id == 8 || $f_id==51 || $f_id==52 ||  $f_id == 19 || $f_id == 17 || $f_id == 18 || $f_id == 20 || $f_id == 21 || $f_id == 22 || $f_id == 44 || $f_id == 48 || $f_id == 60 || $f_id == 73 || $f_id == 74 || $f_id == 83 || $f_id == 84 || $f_id == 85 || $f_id == 86 || $f_id == 87 || $f_id == 89 || $f_id == 91  ) {

                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if( $f_id == 72 && $GrpNo>0 ) {
                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if ( $f_id == 9 || $f_id == 50 )
                                    $AnsFidArr[$GrpNo] = $ans_text;
                                else if ($f_id == 11 || $f_id == 69)
                                    $AnsFidArr[$GrpNo] = $ans_text;
                            }
                        }
                    }
                    if ($lang_id > 1 && $currentQuesId != '') {
                        $getQuesDetailQry = $commonObj->sql_query("SELECT e_test_id as id, lang_id, tid, f_id, question, ques_search, folder_name, ans1, ans2, ans3, ans4, ans5, passage, right1, text, audio_sol FROM e_test_lang WHERE TRIM(question)!='' AND e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "'");
                    }
                    else
                    {
                        $getQuesDetailQry = $commonObj->sql_query("select a.*, b.video from e_test as a
                left join e_test_audio_video as b on a.id=b.e_test_id
                where a.id = '" . $currentQuesId . "'");
                    }
                    $getQuesDetailRow = $commonObj->sql_fetch_assoc($getQuesDetailQry);
                    $rightAns = ltrim(str_replace("^", ",", $getQuesDetailRow['right1']), ",");
                    if ($f_id == 39 || $f_id == 28) {
                        $rightAnsArr = explode(",", str_replace(", ",",",$rightAns));
                    }else{
                        $rightAnsArr = explode(",", $rightAns);
                    }

                    foreach ($rightAnsArr as $key => $val) {
                        $rightAnsArr[$key] = trim(stripslashes(strtolower($val)));
                    }
                    $currentQuesAnsGiven = $quesdata['ans_given'];
                    $currentQuesAnsGiven = ltrim(str_replace("^", ",", $currentQuesAnsGiven), ",");
                    $QuesStr .='<link href="'.constant("CDN").constant("CSS").'/analysis.css?_'.filemtime($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/analysis.css').'" rel="stylesheet" type="text/css" /><style>
                        .Normal_ans.unattempt_ans {
                                        background: #fdedc1;
                                        color: #54932f;
                                }
                            </style>';
                    $ansArr = array();
                    $answ = str_replace('"', '', $getQuesDetailRow['ans1']);
                    $catid = $whichCategory;
                    $getNumOption=$commonObj->sql_result($commonObj->sql_query("select sc.options_in_test from softwares_categories sc 
                                                            LEFT JOIN sbwmd_categories sbc on sbc.id = sc.category
                                                            where sc.testid = '".$testid."' and sbc.topparent = '".$catid."' limit 1"),0);
                    if ($answ != '' && ($getNumOption==0 || $getNumOption>0)) {
                    //    $ansArr['A'] = $ans1 = '<font size="2" face="Arial">' . str_replace('src=wb', $path, $answ) . '</font>';

                        $ansArr['A'] = $ans1 = '<font size="2" face="Arial" style="display:inline-block">'.setImageData($getQuesDetailRow['ans1'],$testid,$getQuesDetailRow['id'],$getQuesDetailRow['folder_name']).'</font>';

                    }
                    $QuesStr .= $commonAnaObj->Type27Div($ansArr['A'], $rightAnsArr, $currentQuesAnsGiven);
                }
            
           }
           else if(in_array($ques_htmlrow['f_id'],$check_fids))
            {
                $get_quesdata = $commonObj->sql_query("SELECT * FROM ques_analysis_new where testtakenid = '".$testtakenid."' and qid = '".$getQuestionsRow['qid']."'");
                $quesdata = $commonObj->sql_fetch_array($get_quesdata);
                $my_ans = str_replace('^', ',', $quesdata['ans_given']);
                $correct_ans = str_replace('^', ',', $quesdata['correct_ans']);
                
                 if($ques_htmlrow['f_id'] == 23 || $ques_htmlrow['f_id'] == 24 || $ques_htmlrow['f_id'] == 25)
                {
                    if($my_ans == 'U')
                    {
                        $my_ans = '-';
                    }
                    $textlable = 'Summary';
                    if($ques_htmlrow['f_id'] == 24){
                        $textlable = 'Essay';
                    }
                $QuesStr .='<strong>Your '.$textlable.':</strong> <br/>'.$my_ans.'<br> 
                          <style>
                                .wrong_ans, .correct_ans, .answer_icon 
                                {
                                    background:none !important;
                                }
                         </style>';
                }
                else {
                    if($_SERVER['REMOTE_ADDR'] != '202.191.175.186'){
                        $QuesStr .='<strong>Your answers:</strong> '.$my_ans.'<br> 
                          <style>
                                .wrong_ans, .correct_ans, .answer_icon 
                                {
                                    background:none !important;
                                }
                </style>';
                    }
                
                }
                if($ques_htmlrow['f_id']==20 && $_SERVER['REMOTE_ADDR'] != '202.191.175.186')
                {
                      $QuesStr .='<style>
                                .answer_icon 
                                {
                                    background:none;
                                }
                         </style>';
                }         
                if($ques_htmlrow['f_id']!=25 && $ques_htmlrow['f_id']!=23 && $ques_htmlrow['f_id']!=24 && $ques_htmlrow['f_id']!=26 && $ques_htmlrow['f_id']!=28 && $ques_htmlrow['f_id']!=20)
                {
                     $QuesStr .='<strong>Correct answers: </strong> '.$correct_ans.'<br>';
                }
                if($_SERVER['REMOTE_ADDR'] == '202.191.175.186')
                {
                    $f_id = $ques_htmlrow['f_id'];
                    $currentQuesId = $getQuestionsRow['qid'];
                    $lang_id = $TestLang;
                    $path = "src=/onlinetest/" . $ques_htmlrow['folder_name'] . "/wb";
                    
                    $groupOptionsFidArr = array(8, 9, 11, 17, 18, 19, 20, 21, 22,44,48,52,51,60,69,72,73,74,83,84,85,86,87,89,91,94);
                    if (in_array($f_id, $groupOptionsFidArr) && $currentQuesId != '') {

                        //if ($_SESSION["userid"] == "445677") {
                            if ($lang_id > 1) {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers_lang 
                                                            WHERE e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "' ORDER BY ans_num, group_ans");
                            } else {
                                $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                            }
                        /*} else {
                            $GetAnsDetailsQry = $commonObj->sql_query("SELECT e_test_id, ans_num AS GrpNo, group_ans AS GrpOpt, ans_text FROM e_test_answers
                                                            WHERE e_test_id = '" . $currentQuesId . "' ORDER BY ans_num, group_ans");
                        }*/
                        if ($commonObj->sql_num_rows($GetAnsDetailsQry) > 0) {
                            while ($GetAnsDetailsRes = $commonObj->sql_fetch_assoc($GetAnsDetailsQry)) {
                                extract($GetAnsDetailsRes);
                                $ans_text = str_replace('src="src="', 'src="', $ans_text);
                                $ans_text = str_replace('"', '', $ans_text);
                                if ($ans_text != '')
                                    $ans_text = str_replace('src=wb', $path, $ans_text);
                                if ( $f_id == 8 || $f_id==51 || $f_id==52 ||  $f_id == 19 || $f_id == 17 || $f_id == 18 || $f_id == 20 || $f_id == 21 || $f_id == 22 || $f_id == 44 || $f_id == 48 || $f_id == 60 || $f_id == 73 || $f_id == 74 || $f_id == 83 || $f_id == 84 || $f_id == 85 || $f_id == 86 || $f_id == 87 || $f_id == 89 || $f_id == 91  ) {

                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if( $f_id == 72 && $GrpNo>0 ) {
                                    $AnsFidArr[$GrpNo][$GrpOpt] = $ans_text;
                                }
                                else if ( $f_id == 9 || $f_id == 50 )
                                    $AnsFidArr[$GrpNo] = $ans_text;
                                else if ($f_id == 11 || $f_id == 69)
                                    $AnsFidArr[$GrpNo] = $ans_text;
                            }
                        }
                    }
                    if ($lang_id > 1 && $currentQuesId != '') {
                        $getQuesDetailQry = $commonObj->sql_query("SELECT e_test_id as id, lang_id, tid, f_id, question, ques_search, folder_name, ans1, ans2, ans3, ans4, ans5, passage, right1, text, audio_sol FROM e_test_lang WHERE TRIM(question)!='' AND e_test_id = '" . $currentQuesId . "' AND lang_id = '" . $lang_id . "'");
                    }
                    else
                    {
                        $getQuesDetailQry = $commonObj->sql_query("select a.*, b.video from e_test as a
                left join e_test_audio_video as b on a.id=b.e_test_id
                where a.id = '" . $currentQuesId . "'");
                    }
                    $getQuesDetailRow = $commonObj->sql_fetch_assoc($getQuesDetailQry);
                    $rightAns = ltrim(str_replace("^", ",", $getQuesDetailRow['right1']), ",");
                    if ($f_id == 39 || $f_id == 28) {
                        $rightAnsArr = explode(",", str_replace(", ",",",$rightAns));
                    }else{
                        $rightAnsArr = explode(",", $rightAns);
                    }

                    foreach ($rightAnsArr as $key => $val) {
                        $rightAnsArr[$key] = trim(stripslashes(strtolower($val)));
                    }
                    $currentQuesAnsGiven = $quesdata['ans_given'];
                    $currentQuesAnsGiven = ltrim(str_replace("^", ",", $currentQuesAnsGiven), ",");
                    if ($f_id==20) {
                        $QuesStr .='<link href="'.constant("CDN").constant("CSS").'/analysis.css?_'.filemtime($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/analysis.css').'" rel="stylesheet" type="text/css" />';
                        $QuesStr .= $commonAnaObj->Type20Div($AnsFidArr,$rightAnsArr,$currentQuesAnsGiven);
                    }
                    else if($f_id==28){
                        $QuesStr .='<link href="'.constant("CDN").constant("CSS").'/analysis.css?_'.filemtime($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/analysis.css').'" rel="stylesheet" type="text/css" />';
                        $QuesStr .= $commonAnaObj->Type28Div($rightAnsArr,$currentQuesAnsGiven);
                    }
                    else if($f_id==26){
                        $QuesStr .='<link href="'.constant("CDN").constant("CSS").'/analysis.css?_'.filemtime($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/analysis.css').'" rel="stylesheet" type="text/css" />';
                        $QuesStr .= $commonAnaObj->Type26Div($rightAnsArr,$currentQuesAnsGiven);
                    }
                    
                    
                    
                }
                
            }

            if($ques_htmlrow['f_id']==27)
            {
                 $QuesStr .= '<style> .answer_icon{background:none !important;} </style>';
            }


        $QuesStr.='</div>            
                    <div class="clear">&nbsp;</div><div class="explanation">';
                    if(isset($explanation) && !empty($explanation)){
                         $QuesStr.='<h2>Right Answer Explanation:</h2>';
                         $QuesStr.='<div>'.$explanation.'</div>
                        
                         ';
                    }
        
         $QuesStr.='</div><div class="clear">&nbsp;</div> <div class="clear">&nbsp;</div>
                <div id="footer1" class="bottom_fix_grey"><div class="wrapper"><ul>';
        if ($Qn != '1') {
            $QuesStr.='<li style="width:40%">
                        <a class="ripplelink" id="previous" onclick="showQueNext(' . $testtakenid . ', ' . ($Qn - 1) . ', '.$testid.');">Back</a>
                      </li>';
        }
        if ($Qn == '1') {
            $QuesStr.='<li style="width:40%">
                        <a class="dull_text ripplelink" id="previous">Back</a>
                       </li>';
        }
        //$QuesStr.='<li><a class="flag_back ripplelink"><i class="flag" style="background:none;"></i></a></li>';
        $QuesStr.='<li style="width:20%"><a class="menu_back ripplelink" onclick="javascript:shw_aque_lst();"><i class="menu"></i></a></li>';
        //$QuesStr.='<li><a class="flag_back ripplelink"><i class="pencil" style="background:none;"></i></a></li>';
        if ($Qn != $quesCount) {
            $QuesStr.='<li style="width:40%">
                        <a class="ripplelink" id="next" onclick="showQueNext(' . $testtakenid . ', ' . ($Qn + 1) . ', '.$testid.');">Next</a>
                       </li>';
        }
        if ($Qn == $quesCount) {
            $QuesStr.='<li style="width:40%">
                            <a class="dull_text ripplelink" id="next">Next</a>
                       </li>';
        }
        $QuesStr.='</ul></div></div>
                </div>';
        echo $QuesStr;
        die();
    }
    ?>
</body>