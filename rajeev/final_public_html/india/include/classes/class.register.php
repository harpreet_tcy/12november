<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/india/misc/misc-classes/SmsTemplates.php';
include_once CLASSES . '/class.layout.php';

class register extends layout {

    public $sms;
    public function __construct() {
        parent::__construct();
        $this->sms = new SmsTemplates();
    }

    /*
     * function name : fullLogin
     * Description : This function include fullLogin
     * Arugments : -
     * Return : It will return html
     * Author : Ashish Khurana
     * Date : 6 july 2013
     * Modify date : -
     */

    function loginHtml($FrmAction, $usertxtemail, $msg = '') {
        $extra_action = '';
        if (isset($_GET['q']) && !empty($_GET['q'])) {
            $extra_action = '?q=' . $_GET['q'];
        }
        $html = '<link href="/india/css/loginPages.css?_' . rand('1212', '47854565') . '" rel="stylesheet" type="text/css"/>
                   <div class="LoginInner">
        	<!-- left part-->
        	<aside>
            	<span class="computerIcon"></span>
                <h3>Succeed in Your Exams</h3>
                <details open>
                    <summary style="display:none;"></summary>
                    <ul>
                    	<li>Get high quality Topic wise, Sectional & Mock <br />Tests.</li>
                        <li>Know where you stand and what next to do to <br />succeed.</li>
                        <li>Customise tests for specific need: Topic and<br />Difficulty Level.</li>
                    </ul>
                </details>
            </aside>
            <!-- left part close-->
            <!--right part-->
            <div class="rightLoginPart">
            	<div class="mainLoginBox">
            		<h2 id="head_part">Login / Sign Up</h2>
                        <div style="width:100%" id="loginError"></div>
                    	<form name="LoginFrm" method="post" onsubmit="return submit_login();" action="/india/tcylogin.php' . $extra_action . '" id="LoginFrm" autocomplete="off">
                            <div id="sh_pwd">
                    		<div class="loginField loginEmailIcon">
                    			<input tabindex="1" type="text" name="txtUsername" id="txtUsername" value="Email / Username" onblur="regChkError(this.id,\'Email / Username\');loginEmptyChk(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Email / Username\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');" onkeypress=" $(\'#txtUsername_error\').hide(); $(\'.loginField\').removeClass(\'errorBorder\');" />
                    		</div><input type="checkbox" style="display:none" value="remember_me" id="remember" name="remember" class="floatLeft" >
                                 <div id="txtUsername_error" class="errorInputMsg">Please enter Email/ Username<span class="errorArrow"></span></div><div class="nextBtnLogin"><a href="javascript:void(0)"><input tabindex="2" type="submit" onclick="return chk_email_addr()" id="lg_check" class="loginBtn" value="Next"/></a></div><input type="hidden" id="txtPassword" name="txtPassword"/>   <input type="hidden" id="destroy_session" name="destroy_session"></div>
                                 
                    	</form>
                    
                </div>
                <div class="socialMediaBox">
                	<div class="hr">
                    	<div class="orOutterCIrcle">
                        	<div class="innerCircle">OR</div>
                        </div>
                    </div>
                    <div class="socialIconBox">
                    	<i style="cursor:pointer" onclick="loginFB();"></i>
                        <p>Login Via Facebook</p>
                    </div>
                    <div class="socialIconBox">
                    	<i style="cursor:pointer" onclick="show_popups();" class="gPlusIcon"></i>
                        <p>Login Via Google Plus</p>
                    </div>
                </div>
            </div>
            <!-- right part close-->
        </div> 

               ';
        return $html;
    }

    function registerHtml() {
        global $commonObj;
        $testimonials = array('Akanksha' => 'The tests on TCYonline are well designed. The detailed analysis highlights our strengths and exposes our weaknesses so that we can improve upon them in time.', 'Pankaj Gupta' => 'I will share a unique experience during my preparation at TCY. I was analyzing my attempt pattern through the time taken per question graph in a test where I couldn\'t perform. When I compared it with my classmate I found he took almost half the time on most of those questions. Next day itself, I contacted him, discussed those questions and understood the shortcomings. This was simply amazing!!!', 'Akshat' => 'I am preparing for my CAT at TCY. I think the TCY- Analytics method helps me a lot in improving my weak sections & the idea of generating tests for weak topics helps me develop concepts in depth. I am confident I can crack CAT.', 'Asheesh Banga' => 'I have used Analytics and test generator features offered on TCYonline.com. In fact, now I use Test Generator quite often. What I like the best about test generator is the flexibility it offers. For Instance, I was surprised to generate test only for reading comprehension based on "Arts" related issues only. What a classification indeed! What else do you want? I now consult teacher just for problem resolution.', 'Palvi Sharma' => 'Thanks a lot. The analytics are extremely brilliant. This is the best website for exam prep.', 'Jason Dsouza' => 'The help provided by TCY Analytics was more than sufficient to score well in GRE. The test material touched upon each and every topic, every test being an accurate indication of your weak points. And once you realize your weak points, your battle is half-won. All in all, my experience with TCYonline has been wonderful.', 'SK MD ARIF' => 'It has always been a challenge to cope with academic pressure and CAT preparations simultaneously. TCYonline provided me with the appropriate blend required to prepare well. Online hand-picked questions helped me prepare at my own convenient time and space, and ensured a steady performance analysis and improvement. I\'ll appear for CAT again and would love to be associated with TCYonline in this phase too.', 'Daniel George' => 'I have scored 334 in the GRE and 115 in TOEFL. Your website was very useful during my preparation for both these tests. Solving a lot of questions of the same difficulty as the actual tests boosted my confidence and helped me perform well on the test day.', 'Akash Verma' => 'To a part i owe my success to tcy. They helped me to get ready for exam by providing insightful mock tests that were almost similar to the actual exams.');
        $key = array_rand($testimonials);
        $value = $testimonials[$key];
        $catDisplay = 'style="display:none"';
        $dropAddClass = '';
        if (isset($_SESSION['SelectedCategory'])) {
            $catDisplay = 'style="display:block"';
            $dropAddClass = 'dropDownAddClass';
        }
        $html .= '<div class="loginPage">
                     <div class="loginAds">
							 <div class="signUpPart registerPageLeft">
                             <h3>Be a part of <font>' . $this->getTotalUsers() . '</font> million+ student family. Ace your exam with the Best Tools and Prep Material.</h3>
                             <div class="signupOptions">
                                     <h2 class="font-Large">Tests</h2>
                                 <h5>Get high quality Topic wise, Sectional & Mock Tests.</h5>
                             </div>
                             <div class="signupOptions ">
                                     <h2 class="font-Large largeAnalytics">TCY Analytics</h2>
                                 <h5>Know your Weak Areas and get Expert Recommendations.</h5>
                             </div>
                              <div class="signupOptions ">
                                     <h2 class="font-Large largeTG">Test Generator</h2>
                                 <h5>Customise tests for specific need: Topic and Difficulty Level.</h5>
                             </div>
                             <div class="signupOptions">
                                     <h2 class="font-Large largeCZ">Challenge Zone</h2>
                                 <h5>Compete on a national level - realtime.</h5>
                             </div>
                             <div class="signupOptions">
                                     <h2 class="font-Large largeDiscuss">Discussions</h2>
                                 <h5>Post and discuss exam related queries.</h5>
                             </div>
                         </div>
                     </div>
                     <form class="form" action="" method="post" id="RegisterFrm" name="RegisterFrm" autocomplete="off" class="RegisterFrmNew">
                     <div class="loginFormBox">
                             <div class="loginForm">
                             <h1>Register Free</h1>
                             <div style="float:left;width:100%"><div style="display:none" class="error-msg " id="registerError1"></div></div>
                             <div class="inputFieldBox fName">
                                    <input onkeypress=" $(\'.errorInputMsg\').hide();" onblur="regChkError(this.id,\'Name\');regChkUser(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Name\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');"  type="text" id="first_name" name="first_name" value="Name"/>
                                    <dd style="display:none"></dd>
                             </div>
                                <div id="first_name_error" class="errorInputMsg">Please enter your name<span class="errorArrow"></span></div>
                             <div class="inputFieldBox">
                                     <input type="text" onkeypress=" $(\'.errorInputMsg\').hide();" name="email_addr" id="email_addr" value="E-Mail" onblur="regChkError(this.id,\'E-Mail\');regChkEmail(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'E-Mail\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');" />
                                 <dd style="display:none"></dd>
                                 <pp id="process1" ></pp>
                             </div>
                                <div id="email_addr_error" class="errorInputMsg">Please enter your email address<span class="errorArrow"></span></div>
                             <div id="check_phone_no" class="inputFieldBox mobileIcon">
                                    <input type="text" onkeypress=" $(\'.errorInputMsg\').hide();" name="phnmber" id="phnmber" value="Mobile" onblur="regChkError(this.id,\'Mobile\');regChkMobile(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Mobile\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');" />
                                    <dd style="display:none"></dd>
                                    <pp id="process2"></pp>
                            </div>
                                <div id="phnmber_error" class="errorInputMsg">Please enter your mobile number<span class="errorArrow"></span></div>
                               <div class="inputFieldBox examCategory" id="dropDownfields">
                                     <input type="text" onkeypress="searchCat_new(this.value)" onclick="register_showCats();" value="Select Maximum 2 Categories" onblur="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');regChkCat(this.id);highlightBorder(this.id,\'blur\');chkChkTick();" onfocus="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');register_showCats();highlightBorder(this.id,\'focus\');" class="internal" name="searchCatTxt" id="register_searchCatTxt">
                                     <i onclick="chkChkTick();$(\'.dropDownCategory\').toggle();$(\'#dropDownOutter\').toggle();"></i>
                                     <dd style="display:none;" id="ticDd" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"></dd>
                                    <div class="ticArow" style="display:none;" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"><img src="/india/images/fieldCorrect.png" /></div>
                                     
                                   <div class="selectCat dropCatgry ' . $dropAddClass . '" id="dropDownOutter" ' . $catDisplay . '>
                                   <div class="dropDownCategory" style="display:none">
                                         <div id="allCategories">';
        $bba = 1;
        $bca = 2;
        $jsfun = '';
        $sql = $commonObj->sql_query("SELECT cat_id, cat_name FROM `main_category` WHERE pid>0 and onlinetest='1' and india='1' and status='1'");

        while ($row = $commonObj->sql_fetch_assoc($sql)) {
            if (isset($_SESSION['SelectedCategory']) && $_SESSION['SelectedCategory'] != '') {
                $j = $_SESSION['SelectedCategory'];
            }
            $i = $row['cat_id'];
            if ($i == 100552 && $row['cat_name'] == 'BBA Entrance') {
                $i = $i . '_' . $bba;
            }
            if ($i == 100552 && $row['cat_name'] == 'BCA Entrance') {
                $i = $i . '_' . $bca;
            }
            if ($i == $j) {
                $checked = 'checked="checked"';
                $jsfun = "<script>chkChkbox_new('cat-$i', '$row[cat_name]');</script>";
                $selectedCat = '<div class="showCtgry" id="cross_' . $j . '">' . $row[cat_name] . '<i onclick="closeUncheckCat_new(\'' . $j . '\');"></i></div>';
            } else {
                $checked = '';
            }
            $html .= '<div class="categoryNameBox" id="catDiv' . $i . '">
                                                         <input type="checkbox"  class="floatLeft register_cate_check" name="courses[]" id="cat-' . $i . '" onclick="return chkChkbox_new(this.id, \'' . $row['cat_name'] . '\')" value="' . $i . '" ' . $checked . ' />
                                                         <p><label for="cat-' . $i . '">' . $row['cat_name'] . '</label></p>
                                                    </div>';
        }
        $html .= '</div>
                                                     </div>' . $jsfun . '
                                      <div class="showSlctCtgry" id="selectdcats" ' . $catDisplay . '>
                                           ' . $selectedCat . '
                                     </div>
                                   </div>
                             </div>
                             <div class="captchaBox">
                                     <img class="captchaImg" src="/india/misc/CaptchaImages.php?characters=4&amp;sid=Math.random&width=128&height=39" id="image"/>
                                 <input type="text" name="captcha" value="" id="captcha" onblur="if($(this).val() == \'\') $(this).addClass(\'errorBorder\'); else $(this).removeClass(\'errorBorder\');$(this).removeClass(\'focus\');regChkCaptcha(this.id,\'' . $_SESSION['security_code'] . '\');highlightBorder(this.id,\'blur\');" onfocus="$(\'.errorInputMsg\').hide();$(this).addClass(\'focus\');$(this).removeClass(\'errorBorder\');"/>
                                 <span><img src="/india/images/captchRefresh.png" onclick="document.getElementById(\'image\').src=\'/india/misc/CaptchaImages.php?characters=4&amp;sid=\'+Math.random()+\'&width=128&height=39\'"/></span>
                             </div>
                            <div class="keepLogin">
                                     <p class="smlFont">By submitting this form, you agree to the <a style="font-size:10px" href="/india/Terms&Conditions.php">Terms</a> & <a style="font-size:10px" href="/india/privacy_policy.php">Privacy Policy</a></p>
                             </div>';
        if (isset($_GET['s']) && $_GET['s'] == 'tcy-opte-register') {
            $html .= ' <input type="hidden" name="tcy-opte-register" value="true" /> ';
        }
        $html .= '<div class="disable" id="login_disable" style="display:none;">Please Wait</div>
                             <input type="submit" class="buttonSbt loginPageButton" value="Signup" id="reg_submit_btn" style="cursor:pointer" onclick="return submit_frm_register();">
                             <div class="ForgotPswd">
                                <a class="floatLeft" href="/login"> Already Registered? Login Here</a>
                             </div>
                         </div>
                     </div>
                     </form>
                     <div class="loginAds login_container_box">
					 	<div class="rightLoginInner">
						<div class="login-btn">
							<a  href="/login">
								<div class="socialButtons margin-top">
									<span class="SocialBtnIcon"><img src="/india/images/login-btn.png"></span>
									<div class="SocialAppName">Already Registered? Login</div>
								</div>
							</a>
						 </div>
                         <div class="signUpPart floatRight rightPArtSignup registerPageRight">
                             <h1>Quick Signup <font style="font-size:30px;"> &#8594;</font></h1>
                                     <a href="javascript:void(0);" onclick="loginFB();">
                                     <div class="socialButtons margin-top">
                                     <span class="SocialBtnIcon"><img src="/india/images/fbBtnIcon.png" /></span>
                                 <div class="SocialAppName">Facebook</div>
                                     </div>
                                     </a>';

        $html .= '<a  onclick="show_popups();">
                                                    <div class="socialButtons gPLuLogin margin-top">
                                                    <span class="SocialBtnIcon"><img src="/india/images/GoogleBtnIcon.png" /></span>
                                                        <div class="SocialAppName">Google</div>
                                                    </div>
                                                </a>';

        $html .= '</div>
                         <div class="signUpPart floatRight rightPArtSignup margin-top registerPageRight">
                             <div class="downArrow"></div>
                             <img src="/india/images/testimonialComa.png" class="floatLeft" style="padding-right:7px;" />
                             <div class="testimoniText">
                                     ' . $value . '
                             </div>
                         </div>
                         <span class="tesimonialName">' . $key . '</span>
                         <!--<h6>96.2% ile</h6>-->
						 </div>
                     </div>
                 </div>';

        return $html;
    }

    function login_registerHtml() {
        global $commonObj;
        $testimonials = array('Akanksha' => 'The tests on TCYonline are well designed. The detailed analysis highlights our strengths and exposes our weaknesses so that we can improve upon them in time.', 'Pankaj Gupta' => 'I will share a unique experience during my preparation at TCY. I was analyzing my attempt pattern through the time taken per question graph in a test where I couldn\'t perform. When I compared it with my classmate I found he took almost half the time on most of those questions. Next day itself, I contacted him, discussed those questions and understood the shortcomings. This was simply amazing!!!', 'Akshat' => 'I am preparing for my CAT at TCY. I think the TCY- Analytics method helps me a lot in improving my weak sections & the idea of generating tests for weak topics helps me develop concepts in depth. I am confident I can crack CAT.', 'Asheesh Banga' => 'I have used Analytics and test generator features offered on TCYonline.com. In fact, now I use Test Generator quite often. What I like the best about test generator is the flexibility it offers. For Instance, I was surprised to generate test only for reading comprehension based on "Arts" related issues only. What a classification indeed! What else do you want? I now consult teacher just for problem resolution.', 'Palvi Sharma' => 'Thanks a lot. The analytics are extremely brilliant. This is the best website for exam prep.', 'Jason Dsouza' => 'The help provided by TCY Analytics was more than sufficient to score well in GRE. The test material touched upon each and every topic, every test being an accurate indication of your weak points. And once you realize your weak points, your battle is half-won. All in all, my experience with TCYonline has been wonderful.', 'SK MD ARIF' => 'It has always been a challenge to cope with academic pressure and CAT preparations simultaneously. TCYonline provided me with the appropriate blend required to prepare well. Online hand-picked questions helped me prepare at my own convenient time and space, and ensured a steady performance analysis and improvement. I\'ll appear for CAT\'14 again and would love to be associated with TCYonline in this phase too.', 'Daniel George' => 'I have scored 334 in the GRE and 115 in TOEFL. Your website was very useful during my preparation for both these tests. Solving a lot of questions of the same difficulty as the actual tests boosted my confidence and helped me perform well on the test day.', 'Akash Verma' => 'To a part i owe my success to tcy. They helped me to get ready for exam by providing insightful mock tests that were almost similar to the actual exams.');
        $key = array_rand($testimonials);
        $value = $testimonials[$key];
        $catDisplay = 'style="display:none"';
        $dropAddClass = '';
        if (isset($_SESSION['SelectedCategory'])) {
            $catDisplay = 'style="display:block"';
            $dropAddClass = 'dropDownAddClass';
        }
        $html .= '<div class="custome_popup"><div class="loginPage" id="popup_login_register">

                     <form class="form" action="" method="post" id="RegisterFrm" name="RegisterFrm" autocomplete="off" class="RegisterFrmNew">
                     <div class="loginFormBox">
                             <div class="loginForm">
                             <!--<center><h4>Register Free</h4></center>-->
                             <div style="float:left;width:100%"><div style="display:none" class="error-msg " id="registerError1"></div></div>
                             <div class="inputFieldBox fName">
                                    <input onkeypress=" $(\'.errorInputMsg\').hide();" onblur="regChkError(this.id,\'Name\');regChkUser(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Name\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');"  type="text" id="first_name" name="first_name" value="Name"/>
                                    <dd style="display:none"></dd>
                             </div>
                                <div id="first_name_error" class="errorInputMsg">Please enter your name<span class="errorArrow"></span></div>
                             <div class="inputFieldBox">
                                     <input type="text" onkeypress=" $(\'.errorInputMsg\').hide();" name="email_addr" id="email_addr" value="E-Mail" onblur="regChkError(this.id,\'E-Mail\');regChkEmail(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'E-Mail\');highlightBorder(this.id,\'focus\');" />
                                 <dd style="display:none"></dd>
                                 <pp id="process1" ></pp>
                             </div>
                                <div id="email_addr_error" class="errorInputMsg">Please enter your email address<span class="errorArrow"></span></div>
                             <div id="check_phone_no" class="inputFieldBox mobileIcon">
                                    <input type="text" onkeypress=" $(\'.errorInputMsg\').hide();" name="phnmber" id="phnmber" value="Mobile" onblur="regChkError(this.id,\'Mobile\');regChkMobile(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Mobile\');highlightBorder(this.id,\'focus\');" />
                                    <dd style="display:none"></dd>
                                    <pp id="process2"></pp>
                            </div>
                                <div id="phnmber_error" class="errorInputMsg">Please enter your mobile number<span class="errorArrow"></span></div>
                               <div class="inputFieldBox examCategory" id="dropDownfields">
                                     <input type="text" onkeypress="searchCat_new(this.value)" onclick="register_showCats();" value="Select Maximum 2 Categories" onblur="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');regChkCat(this.id);highlightBorder(this.id,\'blur\');chkChkTick();" onfocus="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');register_showCats();highlightBorder(this.id,\'focus\');" class="internal" name="searchCatTxt" id="register_searchCatTxt">
                                     <i onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').hide();"></i>
                                     <dd style="display:none;" id="ticDd" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"></dd>
                                    <div class="ticArow" style="display:none;" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"><img src="/india/images/fieldCorrect.png" /></div>
                                     
                                   <div class="selectCat dropCatgry ' . $dropAddClass . '" id="dropDownOutter" ' . $catDisplay . '>
                                   <div class="dropDownCategory" style="display:none">
                                         <div id="allCategories">';
        $bba = 1;
        $bca = 2;
        $jsfun = '';
        $sql = $commonObj->sql_query("SELECT cat_id, cat_name FROM `main_category` WHERE pid>0 and onlinetest='1' and india='1' and status='1'");

        while ($row = $commonObj->sql_fetch_assoc($sql)) {
            if (isset($_SESSION['SelectedCategory']) && $_SESSION['SelectedCategory'] != '') {
                $j = $_SESSION['SelectedCategory'];
            }
            $i = $row['cat_id'];
            if ($i == 100552 && $row['cat_name'] == 'BBA Entrance') {
                $i = $i . '_' . $bba;
            }
            if ($i == 100552 && $row['cat_name'] == 'BCA Entrance') {
                $i = $i . '_' . $bca;
            }

            if ($i == $j) {
                $checked = 'checked="checked"';
                /* $jsfun="<script>chkChkbox_new('cat-$i', '$row[cat_name]');</script>"; */
                $selectedCat = '<div class="showCtgry" id="cross_' . $j . '">' . $row[cat_name] . '<i onclick="closeUncheckCat_new(\'' . $j . '\');"></i></div>';
            } else {
                $checked = '';
            }
            $html .= '<div class="categoryNameBox" id="catDiv' . $i . '">
                                                         <input type="checkbox"  class="floatLeft register_cate_check" name="courses[]" id="cat-' . $i . '" onclick="return chkChkbox_new(this.id, \'' . $row['cat_name'] . '\')" value="' . $i . '" ' . $checked . ' />
                                                         <p><label for="cat-' . $i . '">' . $row['cat_name'] . '</label></p>
                                                    </div>';
        }
        $html .= '</div>
                                                     </div>' . $jsfun . '
                                      <div class="showSlctCtgry" id="selectdcats" ' . $catDisplay . '>
                                           ' . $selectedCat . '
                                     </div>
                                   </div>
                             </div>
                             <div class="captchaBox">
                                     <img class="captchaImg" src="/india/misc/CaptchaImages.php?characters=4&amp;sid=Math.random&width=128&height=39" id="image"/>
                                 <input type="text" name="captcha" value="" id="captcha" onblur="if($(this).val() == \'\') $(this).addClass(\'errorBorder\'); else $(this).removeClass(\'errorBorder\');$(this).removeClass(\'focus\');regChkCaptcha(this.id,\'' . $_SESSION['security_code'] . '\');highlightBorder(this.id,\'blur\');" onfocus="$(\'.errorInputMsg\').hide();$(this).addClass(\'focus\');$(this).removeClass(\'errorBorder\');"/>
                                 <span><img src="/india/images/captchRefresh.png" onclick="document.getElementById(\'image\').src=\'/india/misc/CaptchaImages.php?characters=4&amp;sid=\'+Math.random()+\'&width=128&height=39\'"/></span>
                             </div>
                            <div class="keepLogin">
                                     <p class="smlFont">By submitting this form, you agree to the <a style="font-size:10px" href="/india/Terms&Conditions.php">Terms</a> & <a style="font-size:10px" href="/india/privacy_policy.php">Privacy Policy</a></p>
                             </div>
                             <div class="disable" id="login_disable" style="display:none;">Please Wait</div>
                             <input type="submit" class="loginPageButton" value="Signup" id="reg_submit_btn" style="cursor:pointer" onclick="return submit_frm_register();">
                             <div class="h_seprator"></div>
                             <div class="popUp_Head" style="border:0"><div class="popUp_HeadTxt">Signup With</div></div>
                            <div class="loginForm" style="margin-top:5px;">
                                <div class="floatLeft fullWidth">
                                    <a href="javascript:void(0);" onclick="loginFB();">
                                        <div style="width:45%" class="socialButtons">
                                            <span class="SocialBtnIcon"><img src="/india/images/fbBtnIcon.png"></span>
                                            <div class="SocialAppName">Facebook Connect</div>
                                        </div>
                                    </a>
                                    <a onclick="show_popups();" href="javascript:void(0);">
                                        <div class="socialButtons floatRight gPLuLogin" style="width:45%">
                                            <span class="SocialBtnIcon"><img src="/india/images/GoogleBtnIcon.png"></span>
                                            <div class="SocialAppName">Google Connect</div>
                                        </div>
                                  </a>
                                </div>
                            </div>
                             <!--
                             <div class="ForgotPswd">
                                <a class="floatLeft" href="/login"> Already Registered? Login Here</a>
                             </div>
                             -->
                         </div>
                     </div>
                     </form>';
        /*
          $html.=  '
          <div class="v_seprator"></div>
          <div class="login_box"><form name="LoginFrm" method="post" onsubmit="return chk_login(\'LoginFrm\',\'loginPop\');" action="/india/login.php" id="LoginFrm" autocomplete="off">
          <input type="hidden" id="destroy_session" name="destroy_session">
          <div class="loginFormBox">
          <div class="loginForm">'.$this->forgotPass_html().'
          <center><h4>Login</h4></center>
          <div style="float:left;width:100%" id="loginError"></div>
          <div  id="user_input_outer" class="inputFieldBox">
          <input type="text" name="txtUsername" id="txtUsername" value="Email/ Username" onblur="regChkError(this.id,\'Email/ Username\');loginEmptyChk(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Email/ Username\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');"/>
          </div>
          <div id="txtUsername_error" class="errorInputMsg">Please enter Email/ Username<span class="errorArrow"></span></div>
          <div  class="inputFieldBox passwordIcon">
          <input type="password" name="txtPassword" id="txtPassword" placeholder="Password"   onblur="loginEmptyChk(this.id);highlightBorder(this.id,\'blur\');" onfocus="showErrDiv(this.id);highlightBorder(this.id,\'focus\');"/>
          </div>
          <div id="txtPassword_error" class="errorInputMsg">Please enter password<span class="errorArrow"></span></div>
          <div class="keepLogin">
          <input type="checkbox" value="remember_me" id="remember" name="remember"  class="floatLeft" />
          <p style="cursor:pointer" onclick="remeber_check();">Keep me logged in</p>
          </div>
          <div class="loginBtnOuter">
          <input type="submit" id="loginButton" style="cursor:pointer;" value="Login" class="loginPageButton">
          <div id="login_disable" class="disable">Please Wait</div>
          </div>
          <div class="ForgotPswd">
          <a href="javascript:void(0)" onclick="$(\'.errorInputMsg\').hide();$(\'#loginError\').hide();$(\'#showforgot1\').slideDown(500)" class="floatLeft">Forgot Password?</a>
          <a href="/india/Register/step/1" class="floatRight">Create New Account</a>
          </div>
          </div>
          <div class="clear">&nbsp;</div>
          <div class="loginForm" style="margin-top:5px;">
          <div class="floatLeft fullWidth">
          <a href="javascript:void(0);" onclick="loginFB();">
          <div style="width:45%" class="socialButtons"><span class="SocialBtnIcon"><img src="/india/images/fbBtnIcon.png"></span>
          <div class="SocialAppName">Facebook Connect</div></div>
          </a>
          <a onclick="show_popups();" href="javascript:void(0);">
          <div class="socialButtons floatRight gPLuLogin" style="width:45%"><span class="SocialBtnIcon"><img src="/india/images/GoogleBtnIcon.png"></span><div class="SocialAppName">Google Connect</div>
          </div>
          </a>
          </div>
          </div>
          </div>
          </form>
          <div class="loginAds floatRight">
          <div class="loginPageAd">
          <!-- <div class="bannerimg" id="div-gpt-ad-1414757502259-0" style="width:200px; height:100px;"><script type="text/javascript">
          googletag.cmd.push(function() { googletag.display("div-gpt-ad-1414757502259-0"); });</script></div>-->
          </div>

          </div></div>';
          $html.='</div>';

         */
        $html .= '</div>';
        return $html;
    }
function getVerifyUrl($userid = 0, $type = 0) {
        global $commonObj;
        
        $urlVal = '';
        if($userid  > 0)
        {
            $user_insid = $userid;
            $email = $commonObj->sql_result($commonObj->sql_query("SELECT email FROM sbwmd_members_final WHERE id=".$user_insid),0);
            $type48dataSql= $commonObj->sql_query("SELECT *  FROM `sbwmd_member_stats` WHERE `stat_type` = 48 AND `user_id` ='$userid'");
            if($commonObj->sql_num_rows($type48dataSql) >0 ){
                if( $type==0 ) {
                    $type48dataRow = $commonObj->sql_fetch_assoc($type48dataSql);
                    $email = $type48dataRow['stat_foo'];
                }else{
                    $commonObj->sql_query("UPDATE `sbwmd_member_stats` SET `stat_foo`='".$email."',`date_time`='".date("Y-m-d H:i:s")."' WHERE `stat_type` = 48 AND `user_id` ='".$user_insid."'") or mail('simranjeet.s@tcyonline.com','reg error update',"UPDATE `sbwmd_member_stats` SET `stat_foo`='".$email."',`date_time`='".date("Y-m-d H:i:s")."' WHERE `stat_type` = 48 AND `user_id` ='".$user_insid."'");
                }
            }else{

                $commonObj->sql_query("INSERT INTO `sbwmd_member_stats`(`user_id`, `stat_type`, `stat_val`, `stat_foo`, `date_time`) VALUES ('".$user_insid."',48,0,'".$email."','".date("Y-m-d H:i:s")."')") or mail('simranjeet.s@tcyonline.com','reg error',"INSERT INTO `sbwmd_member_stats`(`user_id`, `stat_type`, `stat_val`, `stat_foo`, `date_time`) VALUES ('".$user_insid."',48,0,'".$email."','".date("Y-m-d H:i:s")."')");
            }
              
            $email_confirm_code=rand(100000, 1500000);
            $queryVal = $commonObj->sql_query("SELECT *  FROM `sbwmd_member_stats` WHERE `stat_type` = 49 AND `user_id` ='$user_insid'");
             if($commonObj->sql_num_rows($queryVal)>0){
                 if($type == 0)
                 {
                     $res = $commonObj->sql_fetch_assoc($queryVal);
		
                     $email_confirm_code = trim($res['stat_foo']);
                 }
                else {
                    $commonObj->sql_query("UPDATE `sbwmd_member_stats` SET `stat_foo`='".$email_confirm_code."',`date_time`='".date("Y-m-d H:i:s")."' WHERE `stat_type` = 49 AND `user_id` ='".$user_insid."'");
                }
                

            }else{
                     $commonObj->sql_query("INSERT INTO `sbwmd_member_stats`(`user_id`, `stat_type`, `stat_val`, `stat_foo`, `date_time`) VALUES ('".$user_insid."',49,0,'".$email_confirm_code."','".date("Y-m-d H:i:s")."')");

            }
                //$to = $_REQUEST['VerifyEmail'];
                 $encypt_email=$email;
                 $sessPage = isset($_SESSION['page']) && $_SESSION['page'] != '' ? '&page=' . base64_encode($_SESSION['page']) : "&page=" . base64_encode("/signup/details");
                $url = "http://" . $_SERVER['SERVER_NAME'] . "/india/tcylogin.php?Confirm=" . $user_insid . "&UserName=" . $email . "&emailverify=yes". "&encypt=" . base64_encode($encypt_email) . "&confirmation_code=" .base64_encode( $email_confirm_code);
                $url .= $sessPage;

                if (isset($_REQUEST['tcy-opte-register']) && $_REQUEST['tcy-opte-register'] == 'true') {
                   $url.="&s=tcy-opte-register";
                }
                $urlVal = $url;
                //$urlArr['orgurl'] = $url;
            //$urlArr['regenrate'] = 1;
            //$urlVal = $commonObj->getUpdateShortUrl($urlArr);
            
              
        }
        return $urlVal;
    }
    function sendVerfyCode($contact_no) {
        global $commonObj,$sendtype;
        $userid = ( isset($_SESSION['userid']) && $_SESSION['userid'] > 0 ) ? $_SESSION['userid'] : ( ( isset($_SESSION['dummyUserId']) && $_SESSION['dummyUserId'] > 0 ) ? $_SESSION['dummyUserId'] : 0 );
        /**********cehck if code already exists************/
        $oldCodeSql = $commonObj->sql_query("select id from mobile_verify where uid = '".$userid."' and mobile = '".$contact_no."' and status='1'");
        if( $commonObj->sql_num_rows($oldCodeSql)>0 ){
            $oldCodeRow = $commonObj->sql_fetch_assoc($oldCodeSql);
            $verifycode = $oldCodeRow['id'];
            if( $_SERVER['REMOTE_ADDR']=='202.191.175.186' ) {
                //mail('rajeev.p@tcyonline.org', 'gett', $verifycode);
            
            }
        }else{
            
            $commonObj->sql_query("insert into mobile_verify set    
                                   uid = '".$userid."' , 
                                   mobile = '".$contact_no."' , 
                                   phone = '".$contact_no."', 
                                   status='1',
                                   added_date='".date('Y-m-d H:i:s')."'");
            
            
            $verifycode = $commonObj->sql_insert_id();
            if( $_SERVER['REMOTE_ADDR']=='202.191.175.186' ) {
                //mail('rajeev.p@tcyonline.org', 'inserted', $verifycode);
            
            }
            
        }
        /**********cehck if code already exists************/
        
//        $_SESSION['VerifyCode'] = rand(999, 9999);
        $_SESSION['verification_code'] = $_SESSION['VerifyCode'] = $verifycode;
        $sentArr = array('verifyCode' => $_SESSION['VerifyCode'], 'MobiNum' => $contact_no);
        //mail('rajeev.p@tcyonline.org', 'verify', 'gjgddddddddj');
        $contactArr = array('919988639725','9988639725','918437371572','8437371572','918566035278','8566035278','917009113127','7009113127');
        if(in_array($contact_no, $contactArr) || 1==1){
           // mail('rajeev.p@tcyonline.org', 'verify', $contact_no);
            $urlArr['orgurl'] = "http://" . $_SERVER['SERVER_NAME'] . "/india/tcylogin.php?Confirm=" . $userid . "&mobilenumber=" . $contact_no . "&phoneverify=yes". "&verifyCode=" . base64_encode($_SESSION['VerifyCode']) ;//$this->getVerifyUrl($userid,$sendtype);
            $urlArr['regenrate'] = 1;
            $urlVal = $commonObj->getUpdateShortUrl($urlArr);
            $result = $this->sms->MobileVerificationCode_New($sentArr,$urlVal,$userid);
        }
        else
        {
            $result = $this->sms->MobileVerificationCode($sentArr);
        }
        
        return $sms_msg;
    }
    
    /*********otp login*********/
    function chkMobileVerificationCode( $code='' ) {
        global $commonObj;
        $code = trim($code);
        if( $code=='' || strlen($code)!=6 ) {
            $code=substr(rand(1111111,9999999).rand(1111111,9999999),0,6);
        }
        $chkSql = "select id,verify_code from mobile_verify where verify_code='".$code."'";
        $chkSql = $commonObj->sql_query($chkSql);
        if( $commonObj->sql_num_rows($chkSql) > 0 ) {
            $code=substr(rand(1111111,9999999).rand(1111111,9999999),0,6);
            return $this->chkMobileVerificationCode($code);
        }
        if( $code!='' && strlen($code)==6 ) {
            return $code;
        }
    }
    function getMverificationCode( $uid,$phone,$vType=1 ){
        ////type = 1 for mobile verification ,2 for otp
        $type = $vType==1 ? 1 : 2;
        global $commonObj;
        if( $uid>0 && $phone>0 ) {
            //////////// check if verfication code exists for same user
            if( $type==2 ) {
               $sqlFields = " id,verify_code "; 
            }else{
                $sqlFields = " id,id as verify_code "; 
            }
            $codeSql = "select ".$sqlFields." from mobile_verify where uid = '".$uid."' and status='1' and type = '".$type."' ";
            $codeSql = $commonObj->sql_query($codeSql);
            if( $commonObj->sql_num_rows($codeSql)>0 ) {
                $codeRow = $commonObj->sql_fetch_assoc($codeSql);
                $code = $codeRow['verify_code'];
            }else{
                $verificationCode = '';
                if( $type==2 ) {
                    $verificationCode = $this->chkMobileVerificationCode();
                }
                if( $verificationCode!='' ) {
                    $insSql = "insert into mobile_verify set verify_code='".$verificationCode."',uid = '".$uid."',mobile = '".$phone."',phone = '".$phone."~',type='".$type."',start_date = '".date('Y-m-d H:i:s')."',end_date='".date('Y-m:d 23:59:59')."'";
                    
                    $commonObj->sql_query($insSql);
                    $code = $newid = $commonObj->sql_insert_id();
                    if( $newid>0 ) {
                        if( $type==2 ) {
                           $code = $verificationCode;
                        }
                    }
                }
            }
        }
        return $code;
    }
    function getb2cOpt($contact_no){
        global $objLayout,$commonObj;
        /**********check valid b2b user***********/
        $smsTmplId = 598191;
        /*********check if sms sent 5 times a day*************/
        $data = $objLayout->validateB2cUser( array('phoneFinal'=>$contact_no) );
        
        if( isset($data['id']) && $data['id'] > 0 ) {
            
            $sqlRestrict = $commonObj->sql_query("select * from tcyonlin_cms.sms_template where phone='" . $contact_no . "' and date(`date`) = '" .date('Y-m-d') . "' AND  template_id = '".$smsTmplId."' ");
            
            if( $commonObj->sql_num_rows($sqlRestrict)>=5 )  {
                return array('success'=>0,"message"=>"You have crossed Maximum attempts for OTP in a day. Please use another medium for Login");
            }else{
            
                $code = $this->getMverificationCode( $data['id'],$contact_no,2 );
                if( $code!='' ) {
                    $sentArr = array('verifyCode' => $code, 'MobiNum' => $contact_no);
                    $result = $this->sms->MobileOTPCode($sentArr);

                    if( strstr(strtolower($result),'success') ) {
                        $trackSql = "insert into tcyonlin_cms.sms_template set phone='" . $contact_no . "' , 
                                    `date` = '" .date('Y-m-d H:i:s') . "' ,  
                                     template_id = '".$smsTmplId."', 
                                     user_id = '".$data['id']."', 
                                     type  = '1' 
                                    ";
                        $commonObj->sql_query($trackSql);
                        return array('success'=>1);
                    }else{
                        return array('success'=>0,"message"=>"");
                    }
                }
            }
        }else{
            return array('success'=>0,"message"=>"This Mobile Number is not Registered with us");
        }
        /**********check valid b2b user***********/   
    }
    /*********otp login*********/
    
    function sendclientVerfyCode($contact_no, $name) {
        $_SESSION['VerifyCode'] = rand(999, 9999);
        $sentArr = array('verifyCode' => $_SESSION['VerifyCode'], 'MobiNum' => $contact_no, 'Client_Name' => $name);
        $result = $this->sms->ClientMobileVerificationCode($sentArr);
        return $sms_msg;
    }

    function step2Html($email, $phone) {
        $chkEmailRedir = $this->emailRedirection($email);
        if ($chkEmailRedir != '')
            $href = 'href="http://' . $chkEmailRedir . '"';
        else {
            $href = 'style="display:none"';
        }

        if (isset($this->hideMobileDiv) && $this->hideMobileDiv === true) {//set from india/Register/registerStep2.php
            $add_style = "style='margin-left:25%;margin-bottom:25px;'";
        } else if (SKIP_MOBILE_STEPS == false) {
            if(isset($this->hideEmailDiv) && $this->hideEmailDiv === true) {
                $add_style = "style='margin-left:25%;margin-bottom:25px;'";
            }else{
                $add_style = "";
            }
            $ind_html = '<div class="sidePart" ' . $add_style . '>
                                        <span class="step2Icon"></span>
                                        <input type="hidden" id="my_mob" value="' . $phone . '"/>
                                        <div id="change_head" class="emailMobileEdit">Verification Code Sent on <strong>' . $phone . '</strong><img src="/india/images/editIconLogin.png" onclick="return change_phone_number();"  /></div>
                                            <div id="sh_data">
                                            <div>
                                                <input onkeypress="$(\'#wrngVerMbl\').hide(); $(\'#verifyCodeIs\').removeClass(\'errorBorder\');" class="mobileNoVerification" type="text" onfocus="placeHolder(this.id,\'Enter verification code\')" onblur="placeHolder(this.id,\'Enter verification code\');" id="verifyCodeIs" value="Enter verification code" name="verifyCodeIs" style="color: rgb(153, 153, 153);">
                                                <div id="wrngVerMbl" class="errorInputMsg" style="display: none;"></div>
                                        </div>
                                        
                                        <input  type="submit" onclick="verifyCode();" id="vrybtn"  value="Submit" class="verificationSubmit">
                                        <input type="button" onclick="return change_phone_number();" value="Resend Code" class="resendCodeBtn" />
                                        <div class="note">Note: This service is currently available for India only. Users subscribed
                        under DND (Do Not Disturb) might not get text message. </div>
                        </div>
                                    </div>';
            if (isset($this->hideEmailDiv) && $this->hideEmailDiv === true) {
                
            }else{
                $ind_html.='<div class="step2MiddleBorder"><div class="orOutter">
                                                <div class="inneror">OR</div>
                                    </div> </div>';
            }
            
        } else {
            //$ind_html = '';
            $add_style = "style='margin-left:25%'";
        }
        $html .= '<main>
                                <div class="LoginInner">
                                        <h1>Verify Your Account</h1>
                                   ' . $ind_html;
        if (isset($this->hideEmailDiv) && $this->hideEmailDiv === true) {
            
            
            
        }else{                       
                        $html .= '<div class="sidePart" ' . $add_style . '>
                                    <span class="step2Icon emailIconStep2"></span>
                                       <input type="hidden" value="' . $email . '" id="ch_email"/>
                                    <div id="email_heading" class="emailMobileEdit">Verification Email Sent on<br /><strong>' . $email . '</strong><img onclick="return change_email_add()" src="/india/images/editIconLogin.png" /></div>
                                        <div id="em_data">
                                            <div class="chkInbox">Please check you inbox</div>
                                            <input type="button" onclick="return change_email_add()" value="Resend Email" class="resendCodeBtn resendemail" />
                                        </div>
                                </div>
                            </div>
                        </main>';
        }
        return $html;
    }

    function getTotalUsers() {
        global $commonObj;
        return $SelectUsers = $commonObj->sql_result($commonObj->sql_query("SELECT ROUND(count(*)/1000000, 2) FROM " . MEMBERTBL_NEW . ""), 0);
    }

    function verficationMail($data) {
        global $commonObj,$sendtype;
        $email = $data['email'];
        $user_insid = $uid = $data['uid'];
        
        $sendtype = 1;
        include_once($_SERVER['DOCUMENT_ROOT']."/india/Register_new/RegisterStep1Mail.php");
        //mail($to, $subject, $body, $headers);
        $phoneFinal = $commonObj->sql_result( $commonObj->sql_query("select phoneFinal from ".MEMBERTBL_NEW." where id = '".$user_insid."'"),0);
        if( isset($phoneFinal) && $phoneFinal>0 && $_SESSION['country_id']<=1 ) {
            $this->sendVerfyCode($_SESSION['mobile']);
            
            if( isset($_SESSION['VerifyCode']) && $_SESSION['VerifyCode']!='' ) {
                //$body = str_replace('<confirmcode>', 'You may be asked to enter this confirmation code : '.$_SESSION['VerifyCode'], $body);
            }
        }
        $commonObj->sendMail($to, $subject, $body, $headers);//all these variables set from above file
        return;
        /*******old code*************/
        $rs_query = $commonObj->sql_query("SELECT * FROM sbwmd_mails WHERE id=33");
        $rs = $commonObj->sql_fetch_array($rs_query);
        $from = $rs["fromid"];
        $from1 = "TCYonline";
        $subject = $rs["subject"];
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: " . $from1 . " <noreply@tcyonline.com> \r\n";
        
        if ($commonObj->sql_num_rows($commonObj->sql_query("SELECT *  FROM `sbwmd_member_stats` WHERE `stat_type` = 48 AND `user_id` ='$uid'")) > 0) {
            $commonObj->sql_query("UPDATE `sbwmd_member_stats` SET `stat_foo`='" . $email . "',`date_time`='" . date("Y-m-d H:i:s") . "' WHERE `stat_type` = 48 AND `user_id` ='" . $uid . "'");
        } else {
            $commonObj->sql_query("INSERT INTO `sbwmd_member_stats`(`user_id`, `stat_type`, `stat_val`, `stat_foo`, `date_time`) VALUES ('" . $uid . "',48,0,'" . $email . "','" . date("Y-m-d H:i:s") . "')");
        }
        $email_confirm_code = rand(100000, 1500000);
        if ($commonObj->sql_num_rows($commonObj->sql_query("SELECT *  FROM `sbwmd_member_stats` WHERE `stat_type` = 49 AND `user_id` ='$uid'")) > 0) {
            $commonObj->sql_query("UPDATE `sbwmd_member_stats` SET `stat_foo`='" . $email_confirm_code . "',`date_time`='" . date("Y-m-d H:i:s") . "' WHERE `stat_type` = 49 AND `user_id` ='" . $uid . "'");
        } else {
            $commonObj->sql_query("INSERT INTO `sbwmd_member_stats`(`user_id`, `stat_type`, `stat_val`, `stat_foo`, `date_time`) VALUES ('" . $uid . "',49,0,'" . $email_confirm_code . "','" . date("Y-m-d H:i:s") . "')");
        }
        $to = $_REQUEST['VerifyEmail'];
        $encypt_email = $email;
        $c_name = $commonObj->sql_result($commonObj->sql_query("SELECT c_name FROM " . MEMBERTBL . " WHERE id=" . $data['uid'] . ""), 0);
        $sessPage = isset($_SESSION['page']) && $_SESSION['page'] != '' ? '&page=' . base64_encode($_SESSION['page']) : "&page=" . base64_encode("/signup/details");
        $url = "http://" . $_SERVER['SERVER_NAME'] . "/india/tcylogin.php?Confirm=" . $uid . "&UserName=" . $email . "&encypt=" . base64_encode($encypt_email) . "&confirmation_code=" . base64_encode($email_confirm_code) . "&emailverify=yes" . $sessPage;
        $url .= $sessPage;

        $MemInMillions = $this->getTotalUsers();
        if ($mail_type == '') {
            $mainMessageTxt = 'You&#39;re just one click away from joining India&#39;s largest testing platform. Verify your Account and be a part of ' . $MemInMillions . ' million-strong student community.';
        } elseif ($mail_type == 'loginForgot') {
            $mainMessageTxt = 'You have tried to login to TCYonline.com, but your email account is yet not verified. Verify your Account and be a part of ' . $MemInMillions . ' million-strong student community.';
        }
        $body = str_replace("<name>", ucwords($c_name), str_replace("<main message>", $mainMessageTxt, str_replace("<link>", $url, str_replace("<student community>", $MemInMillions, $rs["mail"]))));
      
        $commonObj->sendMail($email, $subject, $body, $headers);
      
       // mail($email, $subject, $body, $headers);
       
    }

    function step3html($user_id) {
        global $commonObj,$objLayout;
        $emailVerfiy = $commonObj->sql_result($commonObj->sql_query("SELECT approve FROM sbwmd_members_final WHERE id='" . $user_id . "'"), 0);
        $mobileVerfiy = $commonObj->sql_result($commonObj->sql_query("SELECT verify_email FROM sbwmd_members_final WHERE id='" . $user_id . "'"), 0);
        if (constant(REGISTER_SKIP_VARFY) != true) {
            if ($emailVerfiy == 1 && $mobileVerfiy == 1) {
                $verfiyMsg = '<div class="mobileEmailVerify">
                                 <p><i></i>Mobile &amp; Email Verification is done</p>
                             </div>';
            } else if ($mobileVerfiy == 1) {
                $verfiyMsg = '<div class="mobileEmailVerify">
                                 <p><i></i>Mobile Verification is done</p>
                             </div>';
            } else if ($emailVerfiy == 1) {
                $verfiyMsg = '<div class="mobileEmailVerify">
                                 <p><i></i>Email Verification is done</p>
                             </div>';
            }
            
            $verfiyMsg = '<div class="mobileEmailVerify">
                            <p><i></i>Your Account has been Verified.</p>
                        </div>';
        }

        global $ErrMsg;
        $errDisplay = 'display:none';
        if (isset($ErrMsg) && $ErrMsg != '') {
            $errDisplay = 'display:block';
        }

        //echo "SELECT * FROM " . MEMBERTBL_NEW . " WHERE id=" . $_SESSION['userid'];
        $passDisplay = 'style="display:none;';
        $infoDisplay = 'style="display:none;';
        //echo "SELECT * FROM sbwmd_member_stats WHERE stat_type='44' WHERE user_id='".$_SESSION['userid']."'";
        $passChangedStatus = $commonObj->sql_num_rows($commonObj->sql_query("SELECT * FROM sbwmd_member_stats WHERE stat_type='44' AND user_id='" . $user_id . "'"));
        if ($passChangedStatus > 0) {
            $passDisplay = 'style="display:none;"';
            $infoDisplay = 'style="display:block;"';
            //  die('ab');
        } else {
            $passDisplay = 'style="display:block;"';
            $infoDisplay = 'style="display:none;"';
            // die('c');
        }
        //$userDataQry = $commonObj->sql_query("SELECT * FROM " . MEMBERTBL_NEW . " WHERE id=" . $user_id);
        //$userRes = $commonObj->sql_fetch_assoc($userDataQry);
        $userRes = $objLayout->userdetail($user_id);
        $final_phone_no = $userRes['phoneFinal'];
        $userRes['courses'] = $commonObj->sql_result($commonObj->sql_query(
                        'SELECT cast(group_concat(distinct(b.cat_name) separator "| " ) as char) as courses FROM `j_member_category` a
                               left join sbwmd_categories b on  a.catid=b.id
                               where uid="' . $_REQUEST['user_id'] . '"'), 0);

        $monthArr = array('01' => "January", '02' => "February", '03' => "March", '04' => "April", '05' => "May", '06' => "June", '07' => "July", '08' => "August", '09' => "September", '10' => "October", '11' => "November", '12' => "December");
        if (SKIP_MOBILE_STEPS == false) {
            $strstr = " WHERE type='0'";
        } else {
            $strstr = " WHERE type='1'";
        }
        $QualList = $commonObj->sql_query("SELECT qual, id FROM `tcyonline_qual` " . $strstr . " ORDER BY qual") or die($commonObj->sql_error());
        while ($Qual1 = $commonObj->sql_fetch_array($QualList)) {
            $Qualarray[$Qual1['id']] = $Qual1['qual'];
        }

        $userData = $commonObj->sql_fetch_array($commonObj->sql_query(
                        "select a.nature, a.dob, a.gender, b.name as city, b.id as cityId, a.qual_id, c.id as stateid,c.name as statename,d.id as counrtyid, d.cntry_name as counrtyname from " . MEMBERTBL_NEW . " a 
                LEFT JOIN  ".TABLE_CITIES." b ON a.city=b.id LEFT JOIN ".TABLE_STATES." c on c.id=a.state_non_us LEFT JOIN country_code d on d.id=a.country where a.id='" . $user_id . "'"));
      

        $userData['nature'] = $userData['nature'] == '0' ? 's' : ($userData['nature'] == '1' ? 't' : ($userData['nature'] == '2' ? 'e' : ($userData['nature'] == '3' ? 'st' : ($userData['nature'] == '4' ? 'te' : ($userData['nature'] == '5' ? 'se' : ($userData['nature'] == '6' ? 'ste' : 's'))))));

        $html .= '
					<link href="/india/css/analysis.css?_' . rand('1212', '47854565') . '" rel="stylesheet" type="text/css"/>'
                . '<link href="/india/css/loginPages.css?_' . filemtime('/mobile/css/loginPages.css') . '" rel="stylesheet" type="text/css"/>
                        <div style="width: 100%; float: left; height: 95px;">&nbsp;</div>
                        <div class="step3container" id="passInfo" ' . $passDisplay . '>
                        ' . $verfiyMsg . '
                    <div class="">
         	<div class="loginPage stepContainer">
                <h1>Set Your Password</h1>
               <div class="stepThreefields">
                    <div class="inputFieldBox passwordIcon" id="passDiv">
                    
                        <input type="password" placeholder="Password" name="password" id="password" onblur="regChkError(this.id,\'\');highlightBorder(this.id,\'blur\');showErrDiv(this.id);passEmptyChk(this.id)" onfocus="regChkError(this.id,\'\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');" onkeyup="$(\'#password_error\').hide();">
                    </div>
                    <!--<div id="password_error" class="errorInputMsg" style="display: none;">Please enter password<span class="errorArrow"></span></div>-->
                    <div id="password_error" class="errorInputMsg" style="display: none;">Please set your password<span class="errorArrow"></span></div>
                    <!--<div class="inputFieldBox passwordIcon">
                        	<input type="text" onkeypress="$(this).prop(\'type\',\'password\');" onblur="regChkError(this.id,\'password\');passEmptyChk(this.id);highlightBorder(this.id,\'blur\');showErrDiv(this.id);" onfocus="regChkError(this.id,\'password\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');" value="password" name="confpassword" id="confpassword">
                        
                    </div>-->
                    <!-- daman -->
                    <div class="cls_newChkBox">
                        <input type="checkbox" id="showPass" style="margin-top:10px" class="floatLeft"><label class="proAddNewStdLb marginLftRgt" style="padding:0; margin-left:-5px" for="showPass"><span><span></span></span><b>Show Password</b></label>
                    </div>    
                    <!--<div id="confpassword_error" class="errorInputMsg" style="display: none;">Please enter password<span class="errorArrow"></span></div>-->
                    <div class="loginBtnOuter">
                        <input type="button" onclick="step3PassChng()" class="loginPageButtonNew"  value="Done">
                        <div class="disable" id="login_disable">Please Wait</div>
                    </div>
               </div> 
            </div>
       </div>
    </div>';
        if( $objLayout->chkUserIpInternal() ) {
            
            $html .= '<div class="step3container"><input type="button" id="skipStep3" onclick="skipStep3();"  class="loginPageButtonNew" value="Skip"/></div>';
        }
        $html .= '<form name="updateProfileFrm" id="updateProfileFrm" method="post" action="/signup/details' . ( (isset($_GET['s']) && $_GET['s'] == 'tcy-opte-register' ) ? '/tcy-opte-register' : '' ) . '" autocomplete="off">
     <div class="" id="personalInfo" ' . $infoDisplay . '>
     	
        	<div class="thirdStepbox">';
        if(isset($_SESSION['city_error']) && $_SESSION['city_error']!=''){
            
            $html .= '<div style="font-size:14px;padding:5px 0;text-align:center" class="error-msg fullWidth floatLeft">'.$_SESSION['city_error'].'</div>';
            unset($_SESSION['city_error']);
        }
           
                  $html .= '<div class="loginPage stepContainer">
                  		<h1 class="h1Padding">Your Personal/Academic Information</h1>
                        <div class="simpleText">This information would help us provide you with accurate analysis & reports.
                        </div>
                        <div class="stepThreefields" style="width:425px;">
                             <div class="othersFieldOutter">
                        	 	<div class="DOB" style="padding-top:5px">Gender</div>
                        	<div class="genderOption">
                            	<div class="cls_newRadioBox"><input type="radio" style="margin-top:10px" checked="checked" name="gender" id="male"';

        if ($_REQUEST['gender'] != '' && $_REQUEST['gender'] == 'm') {
            $html .= "checked";
        } else if ($userData['gender'] != '' && $userData['gender'] == "m") {
            $html .= "checked";
        } else {
            $html .= "checked";
        }


        $html .= ' value="m" /><label for="male" class="proAddNewStdLb marginLftRgt" style="margin-left:-35px; padding:0; margin-right:46px"><span><span></span></span><b>Male</b></label></div>
                                <div class="cls_newRadioBox"><input type="radio" name="gender" style="margin-top:10px" id="female"';

        if ($_REQUEST['gender'] == "f") {
            $html .= "checked";
        } else if ($userData['gender'] != '' && $userData['gender'] == 'f') {
            $html .= "checked";
        }


        $html .= ' value="f" /><label for="female class="proAddNewStdLb marginLftRgt" style="padding:0;"><span><span></span></span><b>Female</b></label></div>
                            </div></div>';

        $DOB = isset($userRes['dob']) ? explode("-", $userRes['dob']) : "";

        $html .= '<div class="othersFieldOutter">
                    <div class="DOB">Profile</div>
                    <div class="genderOption">
                        <div class="cls_newChkBox">
                        <input type="checkbox" style="margin-top:10px" name="student" value="s"';

        if (trim($_REQUEST['student']) == '') {
            if (trim($userData['nature']) == "s" || trim($userData['nature']) == "st") {
                $html .= 'checked="checked"';
            }
        } else {
            if (trim($_REQUEST['student']) == "s" || trim($_REQUEST['student']) == "st") {
                $html .= 'checked="checked"';
            }
        }

        $html .= 'id="student" class="floatLeft">
                <label class="proAddNewStdLb marginLftRgt" style="padding:0; margin-left:-5px" for="student"><span><span></span></span><b>Student</b></label>
                </div>
                <div class="cls_newChkBox">
                <input type="checkbox" style="margin-top:10px"  name="teacher" id="teacher" value="t"';
        if ($_REQUEST['teacher'] == '') {
            if (trim($userData['nature']) == "t" || trim($userData['nature']) == "st") {
                $html .= 'checked="checked"';
            }
        } else {
            if (trim($_REQUEST['teacher']) == "t" || trim($_REQUEST['teacher']) == "st") {
                $html .= 'checked="checked"';
            }
        }
        $html .= 'class="floatLeft">
                    <label class="proAddNewStdLb marginLftRgt" style="padding:0;" for="teacher"><span><span></span></span><b>Teacher</b></label>
                    </div>
                </div>
             </div><div class="errorInputMsg" id="profile_error">Please select a Profile<span class="errorArrow"></span></div>';


        $html .= ' <div class="othersFieldOutter">
                           		 <div class="DOB" style="padding-top:10px;">DOB</div>
                            <div class="genderOption floatLeft">
                                 <div class="DOBFiedls" style="margin-right:0;">
                                	<select class="selectYear" id="year" name="year" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                            <option value="">Year</option>';
        for ($i = 1950; $i <= 2008; $i++) {
            $Year = isset($_REQUEST['year']) && $_REQUEST['year'] != '' ? $_REQUEST['year'] : (isset($DOB[0]) && $DOB[0] != '' ? $DOB[0] : '');
            $dayYear = $Year == $i ? "selected" : "";
            $html .= '<option value="' . $i . '" ' . $dayYear . '>' . $i . '</option>';
        }
        $html .= '</select>
                                </div>    
                                <div class="DOBFiedls">
                                	<select name="month" id="month" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                	<option value="">Month</option>';
        foreach ($monthArr as $key => $val) {
            $Month = isset($_REQUEST['month']) && $_REQUEST['month'] != '' ? $_REQUEST['month'] : (isset($DOB[1]) && $DOB[1] != '' ? $DOB[1] : '');
            $dayMonth = $Month == $key ? "selected" : "";
            $html .= '<option value=' . $key . ' ' . $dayMonth . '>' . $val . '</option>';
        }
        $html .= '</select>
                                </div>
                                <div class="DOBFiedls">
                                	<select name="day" id="day" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                	<option value="">Day</option>';
        for ($i = 1; $i <= 31; $i++) {
            $day = isset($_REQUEST['day']) && $_REQUEST['day'] != '' ? $_REQUEST['day'] : (isset($DOB[2]) && $DOB[2] != '' ? $DOB[2] : '');
            $daySel = $day == $i ? "selected" : "";
            // echo $day;
            //echo 'a';
            // die();
            $html .= '<option value="' . $i . '" ' . $daySel . '> ' . $i . '</option>';
        }
        $html .= '</select>
                                </div>

                </div>
                 </div><div style="display: none;" class="errorInputMsg" id="dob_error"> </div>';
         /*******bof ste3 name *******/
        if( !isset($userRes['c_name']) || $userRes['c_name']=='' ) {
            $html .= 
            '
            <div class="inputFieldBox fName">
                                    <input onkeypress=" $(\'.errorInputMsg\').hide();" onblur="regChkError(this.id,\'Name\');regChkUser(this.id);highlightBorder(this.id,\'blur\');" onfocus="regChkError(this.id,\'Name\');showErrDiv(this.id);highlightBorder(this.id,\'focus\');"  type="text" id="first_name" name="first_name" value="Name"/>
                                    <dd style="display:none"></dd>
                             </div>
                                <div id="first_name_error" class="errorInputMsg">Please enter your name<span class="errorArrow"></span></div>';
            
        }
        /*******eof ste3 name*******/
        /*******bof ste3 email*******/
        if( !isset($userRes['email']) || $userRes['email']=='' ) {
            $html .= 
            '
            <div class="inputFieldBox" >
                <input type="text"  name="email_addr" id="email_addr" Placeholder="Email" onblur="regChkEmail(this.id);" onfocus="regChkError(this.id,\'\');showErrDiv(this.id);" value="'.( ( isset($_POST['email']) && $_POST['email']!='' ) ? $_POST['email'] : '' ).'" />
                <dd style="display:none"></dd>   
            </div>
            <div id="email_addr_error" class="errorInputMsg">Please enter your email address<span class="errorArrow"></span></div>';
            
        }
        /*******bof ste3 email*******/
           /*******bof ste3 category *******/
        $catidisexits = $commonObj->sql_num_rows($commonObj->sql_query("select catid from j_member_category where uid='" . $userRes['id'] . "'"));
        if($catidisexits > 0){
            
        }else {
            $html .= 
            ' <div class="inputFieldBox examCategory" id="dropDownfields">
                                     <input type="text" onkeypress="searchCat_new(this.value)" onclick="register_showCats();" value="Select Maximum 2 Categories" onblur="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');regChkCat(this.id);highlightBorder(this.id,\'blur\');chkChkTick();" onfocus="regChkError(this.id,\'Select Your Exam Category (max 2)\',\'Select Maximum 2 Categories\');register_showCats();highlightBorder(this.id,\'focus\');" class="internal" name="searchCatTxt" id="register_searchCatTxt">
                                     <i onclick="chkChkTick();$(\'.dropDownCategory\').toggle();$(\'#dropDownOutter\').toggle();"></i>
                                     <dd style="display:none;" id="ticDd" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"></dd>
                                    <div class="ticArow" style="display:none;" onclick="chkChkTick();$(\'.dropDownCategory\').hide();$(\'#dropDownOutter\').addClass(\'dropDownAddClass\');"><img src="/india/images/fieldCorrect.png" /></div>
                                     
                                   <div style="z-index:99;" class="selectCat dropCatgry ' . $dropAddClass . '" id="dropDownOutter" ' . $catDisplay . '>
                                   <div class="dropDownCategory" style="display:none">
                                         <div id="allCategories">';
        $bba = 1;
        $bca = 2;
        $jsfun = '';
        $sql = $commonObj->sql_query("SELECT cat_id, cat_name FROM `main_category` WHERE pid>0 and onlinetest='1' and india='1' and status='1'");

        while ($row = $commonObj->sql_fetch_assoc($sql)) {
            if (isset($_SESSION['SelectedCategory']) && $_SESSION['SelectedCategory'] != '') {
                $j = $_SESSION['SelectedCategory'];
            }
            $i = $row['cat_id'];
            if ($i == 100552 && $row['cat_name'] == 'BBA Entrance') {
                $i = $i . '_' . $bba;
            }
            if ($i == 100552 && $row['cat_name'] == 'BCA Entrance') {
                $i = $i . '_' . $bca;
            }
            if ($i == $j) {
                $checked = 'checked="checked"';
                $jsfun = "<script>chkChkbox_new('cat-$i', '$row[cat_name]');</script>";
                $selectedCat = '<div class="showCtgry" id="cross_' . $j . '">' . $row[cat_name] . '<i onclick="closeUncheckCat_new(\'' . $j . '\');"></i></div>';
            } else {
                $checked = '';
            }
            $html .= '<div class="categoryNameBox" id="catDiv' . $i . '">
                                                         <input type="checkbox"  class="floatLeft register_cate_check" name="courses[]" id="cat-' . $i . '" onclick="return chkChkbox_new(this.id, \'' . $row['cat_name'] . '\')" value="' . $i . '" ' . $checked . ' />
                                                         <p><label for="cat-' . $i . '">' . $row['cat_name'] . '</label></p>
                                                    </div>';
        }
        $html .= '</div>
                                                     </div>' . $jsfun . '
                                      <div class="showSlctCtgry" id="selectdcats" ' . $catDisplay . '>
                                           ' . $selectedCat . '
                                     </div>
                                   </div>
                             </div>';
            
        }
        /*******eof ste3 category*******/
        $fullcitystatecounn = '';
                if(isset($userData['city'])){
                    $fullcitystatecounn = $userData['city'].', '.$userData['statename'].', '.$userData['counrtyname'];
                }
        $stateidval = '';
                if(isset($userData['stateid'])){
                    $stateidval = $userData['stateid'];
                }
        $cityidval = '';
                if(isset($userData['cityId'])){
                    $cityidval = $userData['cityId'];
                }
        $countyidval = '';
                if(isset($userData['counrtyid'])){
                    $countyidval = $userData['counrtyid'];
                }
        $fullcityname = '';
                if(isset($userData['city'])){
                    $fullcityname = $fullcitystatecounn;//$userData['city'];
                }
        $fullcityname2 = '';
                if(isset($userData['city'])){
                    $fullcityname2 = $userData['city'];
                }
                
                
        $citysateformhtml = '<div class="inputFieldBox CityIcon CityIcon235">
                                <input type="text" Placeholder="City" name="city_list" value="'.$fullcitystatecounn.'" id="city_list"  />
                                <input  type="hidden" id="state" name="state" value="'.$stateidval.'"  />
                                    <input  type="hidden" id="city" name="city" value="'.$cityidval.'" />
                                    <input  type="hidden" id="country" name="country" value="'.$countyidval.'" />
                                    <input  type="hidden" id="fullcity" name="fullcity" value="'.$fullcityname.'" />
                                    <input  type="hidden" id="getCity" name="getCity" value="'.$fullcityname2.'" /></div><div id="state_error" class="errorInputMsg">Please select your City<span class="errorArrow"></span></div><div id="state_error_empty" class="errorInputMsg">Please enter your City<span class="errorArrow"></span></div> <script type="text/javascript"> var forgien="false"; </script>';
        
        
        $mobilefillhtml = '';
        if ($userRes['verify_email'] == '0') {
            if($final_phone_no == 0)
            {
                $final_phone_no = '';
            }
        
            $mobilefillhtml = '<div class="inputFieldBox mobileIcon" style="background-position: 10px -90px; !important";>
                                                <input type="text"  name="phnmber" id="phnmber" Placeholder="Mobile" value="'.$final_phone_no.'" onblur="regChkMobile(this.id);" onfocus="regChkError(this.id,\'\');showErrDiv(this.id);" />
                                                <dd style="display:none"></dd>
                                                <pp id="process2"></pp>
                                                </div>
                                            <div id="phnmber_error" class="errorInputMsg">Please enter your mobile number<span class="errorArrow"></span></div>';
                        }
                        
        if ($catid != 100282) {
            if(isset($_SESSION['cbse_plateform_tests']) && $_SESSION['cbse_plateform_tests']=='1'){
                $html .='<input type="hidden" value="'.$_SESSION['class_id'].'" name="qual" id="qual">';
            }else{
                $html .= '<div class="inputFieldBox qualificationIcon">
                                                    <select name="qual" id="qual" class="R_SignUpF_CityInput blue_class"  >
                                                        <option value="">Qualification</option>';

                foreach ($Qualarray as $qualId => $qualName) {
                    if ($_REQUEST['qual'] == '') {
                        $sel = $userData['qual_id'] == $qualId ? 'SELECTED' : '';
                    } else {
                        $sel = $_REQUEST['qual'] == $qualId ? 'SELECTED' : '';
                    }

                    $html .= '<option value="' . $qualId . '" ' . $sel . '>' . $qualName . '</option>';
                }
                $html .= '</select>
                </div><div class="errorInputMsg" id="qual_error">Please select your Qualification<span class="errorArrow"></span></div>';
                if($_SESSION['country_id'] > 1){
                    if($_SERVER['REMOTE_ADDR'] == '202.191.175.186')
                        {
                            
                        }
                        else
                        {
                            $mobilefillhtml = '';
                        }
                     $html .= $mobilefillhtml.$citysateformhtml.'<script type="text/javascript"> forgien="true"; </script>';
                     
                }
                   

               
            }
            
        }
        if (SKIP_MOBILE_STEPS == false) {

            
if($_SERVER['REMOTE_ADDR'] == '202.191.175.186' || 1==1){
    $html .=$mobilefillhtml.$citysateformhtml;
            
}
 else {
    

             $get_states = $commonObj->sql_query('SELECT * FROM states  where country_id = 99 order by name asc');
            if($commonObj->sql_num_rows($get_states) > 0)
            {

                $html .= '<div class="inputFieldBox CityIcon CityIcon235">
                                <select id="state" name="state" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                         <option value="">State</option>';
                                                    
                    while($state_data = $commonObj->sql_fetch_array($get_states))
                    {
                            $html .="<option value='".$state_data['id']."'>".$state_data['name']."</option>";
                    }

                    $html .="</select></div><div id='state_error' class='errorInputMsg'>Please select your State<span class='errorArrow'></span></div>";
            }
            
            $html .= '<div class="inputFieldBox CityIcon CityIcon235">
                        		<select id="city" name="city" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                         <option value="">City</option>';
            //$citysql = $commonObj->sql_query("SELECT DISTINCT `id`, `name` FROM `city_state` where state_id!='0' group by name ORDER BY name");
            $citysql = $commonObj->sql_query("SELECT DISTINCT a.`id`, a.`name` FROM `city_state` a
                                              LEFT JOIN states b on b.id = a.state_id
                                              where a.state_id!='0' and b.country_id='99' group by name ORDER BY name");
            while ($cityArr = $commonObj->sql_fetch_array($citysql)) {
                $html .= '<option ';
                if ($userData['cityId'] == $cityArr["id"])
                    $html .= ' SELECTED ';
                $html .= ' value="' . $cityArr["id"] . '">' . $cityArr["name"] . '</option>';
            }
            $html .= '</select>
                    		</div><div id="city_error" class="errorInputMsg">Please select your City<span class="errorArrow"></span></div>';
        }
                                 $html .= '<script>
                                    function detectLocation(){
                                        $(\'#detectLoading\').show();
                                        doGeolocation();
                                    }
                                     </script>
                                    <!--<a href="javascript:void(0);" title="Detect Location by google" onclick="detectLocation();" style="float:right; padding-top:5px;">Detect Location</a>-->
                                    <img id="detectLoading" src="/india/images/loadingsmall.gif" style="display:none;  margin-left: 42%;margin-top: 3%;" />
                            <!--<div class="inputFieldBox qualificationIcon">
                        		<select name="qual" id="qual" onblur="regChkSelectError(this.id,\'\');highlightBorder(this.id,\'blur\');" onclick="highlightBorder(this.id,\'focus\');">
                                            <option value="">Qualification</option>';
            foreach ($Qualarray as $qualId => $qualName) {
                $html .= '<option value="' . $qualId . '" >' . $qualName . '</option>';
            }
            $html .= '</select>
                            </div>-->';


            $tempuid = $uid != '' ? $uid : $user_id;
            $Bankcatid = $commonObj->sql_result($commonObj->sql_query("select catid from j_member_category where uid='" . $tempuid . "' and catid in (100241)"), 0);
            if ($Bankcatid > 0) {
                $html .= '<div class="inputFieldBox" style="background-image:none;"> <span style="height:25px; float:left; width:38px;background:url(/india/images/loginFormIcons.png) no-repeat 10px -339px;"></span>';

                $selectBank = '';
                $examBanksql = $commonObj->sql_query("SELECT DISTINCT a.exam_level_id, b.exam_name FROM premium_product_detail as a
                                                                    JOIN exam_levels as b 
                                                                    ON a.exam_level_id=b.id
                                                                    WHERE a.cat_id='" . $Bankcatid . "' AND a.parent_cat_id=0 AND a.status='1' AND a.exam_level_id NOT IN (254,255)  
                                                                    ORDER BY a.priority ASC");

                if ($commonObj->sql_num_rows($examBanksql) > 0) {
                    $selectBank .= '<select id="bankExam" name="bankExam[]" multiple class="R_SignUpF_CityInput blue_class" style="height:80px;margin-left:0">';
                    $selectBank .= '<option value="">Select Exam</option>';
                    while ($examBankres = $commonObj->sql_fetch_array($examBanksql)) {
                        $selectBank .= '<option value="' . $examBankres['exam_level_id'] . '">' . $examBankres['exam_name'] . '</option>';
                    }
                    $selectBank .= '</select>';
                }
                $html .= $selectBank;

                $html .= ' </div>';
            }
            $catid = $commonObj->sql_result($commonObj->sql_query("select catid from j_member_category where uid='" . $tempuid . "' and catid in (100282) "), 0);
            if ($catid > 0) {
                if(isset($_SESSION['cbse_plateform_tests']) && $_SESSION['cbse_plateform_tests']=='1'){
                $html .='<input type="hidden" value="'.$_SESSION['class_id'].'" name="qual" id="qual">';
             }else{
                $html .= '<div class="inputFieldBox selectClsIcon">';

                $select = '';
                $examsql = $commonObj->sql_query("SELECT DISTINCT a.exam_level_id, b.exam_name FROM premium_product_detail as a
                                                                    JOIN exam_levels as b 
                                                                    ON a.exam_level_id=b.id
                                                                    WHERE a.cat_id='" . $catid . "' AND a.parent_cat_id=0 AND a.status='1' AND a.exam_level_id NOT IN (254,255)  
                                                                    ORDER BY a.priority ASC");

                if ($commonObj->sql_num_rows($examsql) > 0) {
                    $select .= '<select id="selectClass" name="selectClass[]"  class="R_SignUpF_CityInput blue_class" >';
                    $select .= '<option value="">Select Class</option>';
                    while ($examres = $commonObj->sql_fetch_array($examsql)) {
                        $select .= '<option value="' . $examres['exam_level_id'] . '">' . $examres['exam_name'] . '</option>';
                    }
                    $select .= '</select>';
                }
                $html .= $select;

                $get_coupan = $commonObj->sql_fetch_array($commonObj->sql_query("SELECT * FROM student_teacher_code WHERE uid='" . $_REQUEST['user_id'] . "' LIMIT 1"));
                if ($get_coupan['teacherid'] > 0) {
                    $teachercode = "TEACH" . $get_coupan['teacherid'];
                }
                $html.='</div>';
            }
                $html .= '<div id="class_error" class="errorInputMsg">Please select class<span class="errorArrow"></span></div>';
                if ($catid == 100282) {
                    $html .= '<div class="inputFieldBox teacherCodeIcon">
                                <input type="text" class="R_SignUpF_Input blue_class" value="' . ($teachercode ? $teachercode : "Teacher Code (Optional)") . '" onblur="placeHolder(this.id,\'Teacher Code (Optional)\');" onfocus="placeHolder(this.id,\'Teacher Code (Optional)\');" name="coupon_code" id="coupon_code" />
                            </div><div class="errorInputMsg" style="' . $errDisplay . '">' . $ErrMsg . '<span class="errorArrow"></span></div>';
                }
            }
            $sqlc = "SELECT * FROM sbwmd_member_stats WHERE user_id='".$user_id."' and stat_type='26'";
          //  if($user_id == '4314496')
            //{
                //mail('rajeev.p@tcyonline.org', 'sql1', $sqlc);
           // }
            $reservationCategory = $commonObj->sql_fetch_array($commonObj->sql_query($sqlc));
            $html .= '<div class="inputFieldBox ReserIcon reservCat">
                                    <select name="reservationCategory" id="reservationCategory">
                                     <option value="">Any reservation category? (Optional)</option>';
            $resultUC = $commonObj->sql_query("select * from sbwmd_category_master where show_status='1' order by display_order asc");
            while ($dataUC = $commonObj->sql_fetch_array($resultUC)) {
                $checkeddd = '';
                if($reservationCategory['stat_val'] == $dataUC['id'])
                {
                   $checkeddd = 'SELECTED';
                }
                $html .= '<option '.$checkeddd.' value="' . $dataUC['id'] . '" >' . $dataUC['categoryTitle'] . '</option>';
            }
            $html .= '</select>
                            </div>';

            $html .= '<div class="compEmp">
                        Whether any of your friends/relatives is directly or indirectly employed or has ever been employed with TCY. Yes/No <br/>If yes, please give details in the box below
                        <div class="inputFieldBox">
                            <textarea name="compEmp"></textarea>
                        </div>
                    </div>';


            // if($_SERVER['REMOTE_ADDR']=="122.160.245.19"){
            $catMbaid = $commonObj->sql_result($commonObj->sql_query("select catid from j_member_category where uid='" . $tempuid . "' and catid in (100000) "), 0);
            if ($catMbaid > 0) {
                $sqly = "select id,stat_foo from sbwmd_member_stats where stat_type='39' and user_id='" . $user_id . "'";
                $mbaExamyr = $commonObj->sql_fetch_array($commonObj->sql_query($sqly));
                $apprYrArr = explode(",", $mbaExamyr['stat_foo']);
                $html .= '<div class="othersFieldOutter">
                        	 	<div class="DOB" >Appearing in</div>
                                            <div class="genderOption">';
                $selectMba = '';
                $currentYear = date("Y");
                $nextYear = date('y', strtotime('+1 year', strtotime($currentYear)));
                $year = 1;
                while ($year <= 3) {
                    $checkedVal = '';
                    if(in_array($currentYear . "-" . $nextYear, $apprYrArr))
                    {
                        $checkedVal='checked="checked"';
                    }
                    $html .= "<div class='cls_newChkBox'><input ".$checkedVal." class='floatLeft' style='margin-top:10px' type='checkbox' name='mbaExam[]' class='floatLeft' value='" . $currentYear . "-" . $nextYear . "' id='" . $currentYear . "-" . $nextYear . "'><label class='proAddNewStdLb marginLftRgt' style='padding:0;' for='" . $currentYear . "-" . $nextYear . "'><span><span></span></span><b>" . $currentYear . "-" . $nextYear . "</b></label></div>";
                    $currentYear++;
                    $nextYear++;
                    $year++;
                }

                $html .= '</div></div>';
            }
        }

        $html .= '<a href="javascript:void(0);" onclick="submit_info();"  class="loginPageButtonNew">Finish</a>
                        </div>
        	</div>
        </div> 
     </div> ';
        $html .= '</form>';
        return $html;
    }

    function rightSideHtml_register($uid) {
        $pageUrl = "registerStep3Query";
        $MemcacheResult = $commonObj->sql_result($commonObj->sql_query("SELECT otherdata FROM `tcyMemcache` WHERE pageUrl='" . $pageUrl . "' AND cachDt>'" . date('Y-m-d') . "'"), 0);
        $userRes['courses'] = $commonObj->sql_result($commonObj->sql_query('SELECT cast(group_concat(distinct(b.cat_name) separator "| " ) as char) as courses FROM `j_member_category` a
                                                                      left join sbwmd_categories b on  a.catid=b.id
                                                                      where uid="' . $uid . '"'), 0);
        if ($MemcacheResult != '') {
            $otherData = $MemcacheResult;
        } else {
            $MemInMillions = $this->getTotalUsers();
            $AnalyseAtt = $commonObj->sql_result($commonObj->sql_query("SELECT SUM(tot_attempt) FROM analyse_total_final"), 0);
            $QuizAtt = $commonObj->sql_result($commonObj->sql_query("SELECT count(*) FROM `livequizscore`"), 0);
            $totalAtt = number_format($AnalyseAtt + $QuizAtt);
            $totChlng = number_format($commonObj->sql_result($commonObj->sql_query("SELECT count(*) FROM chlng_test"), 0));
            $totGen = number_format($commonObj->sql_result($commonObj->sql_query("SELECT count(DISTINCT(userid)) FROM Gen_detail"), 0));
            $otherData = $totalAtt . "-" . $totChlng . "-" . $totGen . "-" . $QuizAtt . "-" . $MemInMillions;
            $InsertPremium = "INSERT INTO `tcyonlin_tcybeta`.`tcyMemcache` SET 
                                             `pageUrl` = '" . $pageUrl . "', `result` = '', otherdata='" . trim($otherData) . "',
                                             `cachDt` = '" . date('Y-m-d', strtotime('+2 days')) . "'
                                              ON DUPLICATE KEY UPDATE  otherdata='" . $otherData . "',
                                              `cachDt` = '" . date('Y-m-d', strtotime('+2 days')) . "',countRun=countRun+1";
            //if($_SESSION['userid']=='1026467') echo $InsertPremium ;
            $commonObj->sql_query($InsertPremium);
        }
        //memcache for queries
        $otherDataArr = explode("-", $otherData);
        $html .= '<div class="step2_bottom">
                                        <h2>As a member of ' . $otherDataArr[4] . '+ MILLION - Student family,you will get</h2>
                                        <div class="descBox">
                                            <h3>Online Tests</h3>
                                             <p>' . str_replace("|", " |", $userRes['courses']) . '</p>
                                            <span>' . $otherDataArr[0] . ' questions attempted so far</span>
                                    </div>
                                    <div class="descBox secondIcon">
                                            <h3>CHALLENGE ZONE</h3>
                                        <p>Play, Compete &amp; Learn real time with others</p>
                                        <span>' . $otherDataArr[1] . ' challenges taken so far</span>
                                    </div>
                                    <div class="descBox thirdIcon">
                                            <h3>test Generator</h3>
                                        <p>Customize your preparation by creating your own tests</p>
                                        <span>' . $otherDataArr[2] . ' students served so far</span>
                                    </div>
                                    <div class="descBox fourthIcon">
                                            <h3>Quiz Zone</h3>
                                        <p>Fun \'n\' Learn with Multi-Player quizzes on any topic</p>
                                        <span>' . number_format($otherDataArr[3]) . ' questions played so far</span>
                                    </div>
                                  </div>';
        return $html;
    }

    private function forgotPass_html() {
        $html = '<div id="showforgot1" class="forgetPassPopup">
                                <h1>Password Recovery</h1>           
					<div  id="email_forget" class="inputFieldBox">				
                                                    <input class="internal" type="text" name="email" id="email"  onblur="registerOnblur(this.id);hide_Shadow(this.id);chkPlaceholder_focus(this.id);regChkError(this.id,\'Via Email\');frgtEmail(this.id,\'blur\');" value="Via Email" onFocus="chkField(this.id);show_Shadow(this.id);chkPlaceholder_focus(this.id);regChkError(this.id,\'Via Email\');frgtEmail(this.id,\'focus\');"  />
                                                    <pp id="forgot_pass"></pp>
					            <div onClick="fieldfoucs_span_click(\'phone_password\');" class="rightSideIcons" id="email_nameSpan"></div>
					            </div>                                   
                                                    <div class="userField mobileIconPosition"  id="mbl_forget" style="display:none"><input class="internal" type="text" name="phone_password" id="phone_password" value=""  onblur="registerOnblur(this.id);hide_Shadow(this.id);chkPlaceholder_focus(this.id);" placeHolder="Mobile" onFocus="chkField(this.id);show_Shadow(this.id);chkPlaceholder_focus(this.id);"  style="width:217px"/>
                                                          <span onClick="fieldfoucs_span_click(\'phone_password\');" class="rightSideIcons" id="mbl_nameSpan"></span>
                                                    </div>
                                      <!--<div id="recoverPass_msg" class="wrongEmail" style="display:block"></div>-->
                                      <div id="recoverPass_msg" class="errorInputMsg wrongEmail" style="display:none">Please enter a valid email<span class="errorArrow"></span></div>
                                      <div class="clear">&nbsp;</div>
                                        <input type="button" onClick="password_recovery();" style="margin-right:10px;" class="buttonSbt floatLeft" value="Retrieve" name="sbt"> 
                                        <input type="button" onClick="$(\'#showforgot1\').slideUp(500)" class="greyButton floatLeft" value="Close" name="cancelbtn">
                                      
                           </div>';
        return $html;
    }

    function emailRedirection($email) {
        $emailPart = explode('@', $email);
        $returnVal = '';
        $arrayEmail = array('yahoo' => 'yahoomail.com', 'ymail' => 'yahoomail.com', 'gmail' => 'gmail.com', 'hotmail' => 'hotmail.com', 'rediff' => 'rediffmail.com');
        foreach ($arrayEmail as $key => $value) {
            if (strstr($emailPart[1], $key)) {
                $returnVal = $value;
                break;
            }
        }
        return $returnVal;
    }

    function register_header() {
        if ($_GET['ajaxType'] != '1') {
            $header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                 <html xmlns="http://www.w3.org/1999/xhtml">
                                    <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                        <meta name="description" content="Register Free and Learn with TCYonline, Get Unlimited Free Online Tests and Courses, Generate your own test, Challenge your competitors, Play Quiz, Analyze yourself with TCYanalytics to prepare any exam" />
                                        <meta name="keywords" content="register free, tcyonline free registration, tcyonline, tcy, tcyonline registration, tcy registration, register to learn" />
                                        <title>TCYonline Registration: Register Free to Learn online | Avail Free Online Tests &amp; Courses</title>';
            $header .= $this->jquery();
            $header .= $this->commonObj->get_include_contents(constant("INCLUDE") . "/scriptfiles.php");
            $header .= '<script  src="' . JS . '/registration.js?_' . filemtime($_SERVER['DOCUMENT_ROOT'] . constant('JS') . '/registration.js') . '"></script>
                                                    <link href="' . CSS . '/login_register.css?_' . ($_SERVER['DOCUMENT_ROOT'] . constant('CSS') . '/login_register.css') . '" rel="stylesheet" type="text/css"/> ';
            $header .= '</head>
                                    <body>' . $this->commonObj->get_include_contents(constant("SITE_HEADER"));
            $header .= '<div class="wrap_container">
                                                        <div class="wrap">';
            return $header;
        }
    }

    function register_footer() {
        if ($_GET['ajaxType'] != '1') {
            $googleTracking = true;
            $header .= '</div>
                                         </div>' . $this->commonObj->get_include_contents(constant("SITE_FOOTER")) . "</body></html>";
        }
        return $header;
    }
    function getMobileVerificationPopup() {
        $html = '<style type="text/css">
            * {
                    padding: 0;
                    margin: 0
            }
            .otp-bg {
            
                    background: #fff;
                    border-radius: 5px;
                    box-shadow: 1px 2px 10px rgba(153, 153, 153, 0.9);
                    position: absolute;
                    margin: 0 auto;
                    right: 0;
                    top: 50%;
                    width: 400px;
                    height: 350px;
                    left: 50%;
                    margin: -175px 0 0 -229px;
                    z-index: 2;
            }
            .otp-mob {
                    background: -moz-linear-gradient(left, #28cc88 0, #17bdbb 47%, #01aafc 100%);
                    background: -webkit-linear-gradient(left, #28cc88 0, #17bdbb 47%, #01aafc 100%);
                    background: linear-gradient(to right, #28cc88 0, #17bdbb 47%, #01aafc 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#28cc88\', endColorstr=\'#01aafc\', GradientType=1 );
                    padding: 18px;
                    color: #fff;
                    border-top-left-radius: 5px;
            }
            .otp-text {
                    padding: 15px 15px 0px;
                    font-size: 16px;
                    color: #444;
                    text-align: justify;
            }
            .otp-input {
                    text-align: center;
                    padding-top: 15px;
            }
            .otp-input input[type="text"] {
                    width: 50%;
                    padding: 8px 8px;
                    margin: 8px 0;
                    box-sizing: border-box;
                    border: 1px solid #555;
                    outline: none;
                    display: inline-block;
            }
            .otp-edit-icon {
                    vertical-align: middle;
                    padding-left: 11px;
                    padding-top: 9px;
            }
            .otp-verify, .otp-verify-text {
                    text-align: center;
            }
            .otp-verify p {
                    color: #444;
                    font-size: 18px;
                    padding-top: 15px;
                    font-weight: 700;
            }
            .otp-verify-text {
                    font-size: 18px;
                    padding-top: 15px;
            }
            .otp-btn {
                    text-align: center;
                    padding-top: 15px;
            }
            .otp-resend {
                    padding-left: 5px;
                    color: #0079ff;
            }
            .otp-input-1 {
                    text-align: center;
                    padding-top: 15px;
            }
            .otp-input-1 input[type="text"] {
                    border-bottom: 1px solid #000;
                    border-top: none;
                    border-right: none;
                    border-left: none;
                    width: 7%;
                    text-align: center;
            }
            </style>
            <div id="otp-verification" class="otp-bg" style="display:none;">
                <div class="otp-mob">
                  <h3>Mobile Verifcation</h3>
                </div>
                <a href="javascript:void(0);" class="adjestclose closeClassSty" onclick="$(\'#otp-verification\').hide();$(\'#dulldivforotp\').hide();"><img src="/india/images/cross-icon1.png" alt="close" width="15" height="15"></a>
                <div class="otp-text">
                  <p >One Time Password (OTP) has been sent your mobile number</p>
                  <p id="alertmsg"></p>
                  
                </div>
                <div class="otp-input">
                  <form>
                    <input id="verifynumberid" disabled="true" type="text" onblur="regChkMobile_otp(this.id);">
                    <span class="otp-edit-icon"><a style="cursor: pointer;" onclick="editmobilenum();"><img src="/site_images/edit-icon.png"></a></span>
                  </form>
                </div>
                <div class="otp-verify">
                  <p>Verify OTP</p>
                </div>
                <div class="otp-input-1">
                  <form>
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">
                    <input name="verificationcodetxt[]" class="verificationcodetxt" onkeyup="verifymobilecode()" maxlength="1" type="text">

                  </form>
                </div>
                <div class="otp-verify-text">
                  <p>Didn\'t get OTP?<a style="cursor: pointer;" onclick="sendVerificationCode_new();" class="otp-resend">Resend OTP</a></p>
                </div>
              </div><div id="dulldivforotp" style="display:none; background: #000000; filter: alpha(opacity=70); height: 100%; opacity: .7; position: fixed; width: 100%; left: 0; top: 0; z-index: 1;">
        </div><script>
            function editmobilenum(){
                $("#verifynumberid").prop("disabled",false);
                $("#verifynumberid").focus();
            }
              function openverificationPopUp(){
                    $("#verifynumberid").val($("#phnmber").val());
                    sendVerificationCode_new();
                    $("#otp-verification").show();
                    $("#dulldivforotp").show();
              }
              $(function(){
                $("body").on("keyup", "input.verificationcodetxt", function(event)
                {
                  var key = event.keyCode || event.charCode;
                  var inputs = $("input.verificationcodetxt");
                  if(($(this).val().length == 1) && key != 32)
                  {
                    inputs.eq(inputs.index(this) + 1).focus();  
                  } 
                  if( key == 8 || key == 46 )
                  {
                    var indexNum = inputs.index(this);
                    if(indexNum != 0)
                    {
                    inputs.eq(inputs.index(this) - 1).val("").focus();
                    }
                  }

                });
              });
              </script>';
        
        return $html;
    }
}
